## Configuration Docker

### Configuration du Docker daemon (dockerd)

La configuration du Docker daemon se fait à l'aide d'un fichier au format JSON qui nécessite d'être crée lors d'une nouvelle installaton car il n'existe pas par défaut. 

Ce fichier est situé ici:

- Linux: **/etc/docker/daemon.json**
- Windows **C:\ProgramData\Docker\config\daemon.json** ou à l'aide de l'application Docker -> Paramètres -> Daemon

Pour plus de détails concernant les instructions disponibles dans un fichier `daemon.json` vous pouvez vous rendre sur la page de [référence](https://docs.docker.com/engine/reference/commandline/dockerd/) sur le site de Docker.

Notez que tous les changements apportés à ce fichier nécessite le redémarrage du service docker

### Emplacement des composants Docker
Par défaut les images, les containers et les volumes sont localisés dans le répértoire suivant:

- Linux: `/var/lib/docker`
- Windows: `/var/lib/docker` également car Docker tourne dans une VM sur HyperV

Pour changer l'emplacement par défaut de ces composants Docker, vous pouvez ajouter une entrée dans le fichier `daemon.json`:
```
{
    "data-root": "/mnt/docker"
}
```
Pour Windows, vous pouvez changer également l'emplacement par défaut en indiquant un dossier partagé de l'hôte sur la VM.


### Configurer un log-driver

Vous pouvez définir quel **log-driver** doit être utilisé par défaut pour tous les containers qui sont démarrés sur l'hôte en spécifiant une entrée dans le fichier `daemon.json`

```
{
  "log-driver": "syslog"
}
```

Suivant le **log-driver** utilisé vous pouvez configurer des options comme par exemple avec le **log driver** par défaut **json-file** :
```
{
  "log-driver": "json-file",
  "log-opts": {
    "max-size": "10m",
    "labels": "production_status",
    "env": "os,customer"
  }
}
```

Vous pouvez consulter la liste des **log-driver** disponible et leur configuration sur la [page](https://docs.docker.com/config/containers/logging/configure/#supported-logging-drivers) de référence sur le site de docker

### Configurer un storage-driver
Vous pouvez définir quel **storage-driver** doit être utilisé par les containers Docker en ajoutant l'option suivante dans le fichier `daemon.json`:
```
{
  "storage-driver": "overlay2"
}
```
De plus amples informations sur les options de stockage seront détaillés dans une [section](../storage/README.md) dédiée. 

Pour terminer, veuillez noter que suivant les configurations ajoutées au fichier `daemon.json` il est important de les faire dès le début pour éviter de devoir migrer les composants Docker d'un disque à l'autre.