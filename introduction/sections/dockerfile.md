## 2.0 Création d'une image

### Dockerfile

La création d'une image nécessite l'utilisation d'un fichier nommé **Dockerfile**

A quoi ressemble un Dockerfile ?

```
FROM ubuntu:trusty

RUN apt-get update &&\
    apt-get install -y wget apache2 curl unzip&&\
    apt-get clean&&\
    rm -rf /var/lib/apt/lists/*

EXPOSE 80 443

CMD ["apachectl", "-D", "FOREGROUND"]

```

#### Build d'une image avec un Dockerfile

Il est possible de construire une image avec la commande **docker build**, en utilisant un fichier Dockerfile et un ***contexte***.

Il existe 3 approches pour le contexte: 
- Construction avec un contexte par chemin (**PATH**). Option courante: **.**
- Construction avec un contexte par URL (repository **git** par exemple, et le contexte sera le repository **git**)
- Construction avec flux d'entrées (**STDIN**) en utilisant l'option: **-** (Cette dernière approche ne prends pas en compte de contexte)

Example avec la construction d'une image avec un contexte chemin
```
docker build -t myapache:v1 .
```

La commande ci dessous va créer une image se nommant myapache avec un **tag** v1 (option **-t**).

La commande **build** va par défaut tenter de trouver un fichier **Dockerfile** relatif à l'exécution de la commande build, il est possible d'indiquer une autre nom de fichier avec l'option **-f** ou **--file** 

```
docker build -t myapache:v1 -f MonDockerFile .
```

Pour plus de détails concernant les options de la commande **build** vous pouvez vous rendre sur la page de [référence](https://docs.docker.com/engine/reference/commandline/build/) sur le site de Docker

#### Quelques instructions Dockerfile

##### Instruction **ARG**
L'instruction **ARG** vous permet de définir des variables qui peuvent être utilisés comme paramètres au moment de la construction de l'image:
```
docker build -t myimage:v1 --build-arg ARG_NAME=<une_valeure> .
```

Cette commande peut également être placée avant l'instruction **FROM** mais ne sera utilisable que dans l'instruction **FROM**, si vous voulez utilisez une instruction **ARG** dans la suite du Dockerfile, vous devez le placer après l'instruction FROM
```
    ARG TAG_VERSION=6
    FROM centos:${TAG_VERSION}
    ...
```

##### Instruction **ENV**
L'instruction **ENV** vous permet de définir une variable d'environnement sous la forme suivante:
\<clef\> \<valeur\> ou \<clef\>=\<valeur\>
```
     ENV ENVIRONMENT="production"
     ENV VERSION "production"
```

Les variables d'environnement défini en utilisant l'instruction **ENV**, vont rester présent lorsqu'un container est instancié, et seront visible avec la commande **docker inspect**. Elles peuvent être surchargées dans la commande **docker run --env \<clef\>=\<valeur\>**

##### Instruction **COPY**
L'instruction **COPY** vous permet de copier des fichiers depuis le contexte local dans l'image, et ne fonctionne qu'avec des fichiers.
```
    COPY <src> <dest> (ajouter des guillemets si des espaces existent)
    COPY index.html /var/www/html
    WORKDIR - used as variable for relative path
```

##### Instruction **ADD**
L'instruction ADD copie de nouveaux fichiers, dossiers ou fichiers distants (URL) dans l'image.
```
    ADD <src> <dest> (ajouter des guillemets si des espaces existent)
```
Si le fichier est une archive locale compressée (gzip,bzip) il sera automatiquement décompressé à la destination sur l'image.

##### Instruction **EXPOSE**
Cette instruction ne publie pas en réalité le port, cela fonctionne comme un type de documentation entre la personne qui construit l'image et la personne qui exécute le container pour indiquer quels ports sont suscpetibles d'êtres publiés. Vous pouvez utiliser deux protocoles TCP ou UDP. Par défaut le protocole TCP est utilisé.

```
EXPOSE 80 443
EXPOSE 80/udp
EXPOSE 80/tcp
```
Lors de la commande `docker run` vous pouvez définir quels sont les ports à mapper sur les ports exposés du container à l'aide de l'option `-p`, comme par exemple :
```
docker run -d --name mynginx -p7777:80 nginx
```
Dans ce cas, les instructions **EXPOSE** du Dockerfile ne sont pas utilisés et sont ici plus à titre informatif, cependant si on utilise l'option `-P` lors de la commande `docker run` les ports définis dans le Dockerfile avec l'instruction **EXPOSE** seront automatiquement mappés sur des ports aléatoires et libre sur l'hôte.

##### Instructions **CMD**
Commande à exécuter par défaut lorsque le container démarre. Une seule instruction **CMD** par Dockerfile, si plusieurs sont définis, seulement le dernier défini sera pris en compte.

Cette instruction n'est pas exécutée au moment de la construction de l'image, mais seulement au moment du démarrage du container, et elle peut également être surchargé au démarrage du container

```
ARG TOMCAT_VERSION=8.5.34-jre8-alpine

FROM tomcat:${TOMCAT_VERSION}

EXPOSE 8080

CMD ["catalina.sh", "run"]
```

##### Instruction **ENTRYPOINT**
L'instruction **ENTRYPOINT** permet de lancer le container comme un exécutable. Elle référence en générale un fichier de script qui permet d'initialiser le container de la manière attendue, avant de donner la main à la commande par défaut ou surchargée dans l'instruction de démarrage.
```
ARG TOMCAT_VERSION=8.5.34-jre8-alpine

FROM tomcat:${TOMCAT_VERSION}

COPY docker-entrypoint.sh /
RUN ["chmod", "+x", "/docker-entrypoint.sh"]
ENTRYPOINT [ "/docker-entrypoint.sh" ]

CMD ["catalina.sh", "run"]
```

Cette instruction n'est pas exécutée au moment de la construction de l'image, mais seulement au moment du démarrage du container, et elle peut également être surchargé au démarrage du container.

Elle est utilisée pour l'initialisation et configuration nécessaire au bon fonctionnement de l'applicatif dans le container. 

Il est courant que les instructions **CMD** et **ENTRYPOINT** sont utilsés en conjonction. L'instruction **ENTRYPOINT** s'exécutera à la place de la commande (**CMD**) et recevra la commande (**CMD**) en tant qu'arguments.

Pour plus de détails concernant les instruction disponibles dans un Dockerfile vous pouvez vous rendre sur la page de [référence](https://docs.docker.com/engine/reference/builder/) sur le site de Docker.

#### Instruction **ONBUILD**

Permet de définir des instructions qui ne seront executées que par des images "enfants" utilisant l'image parente ayant des instructions **ONBUILD**

Exemple:
```
FROM node:0.0.0-stretch

RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app

ONBUILD ARG NODE_ENV
ONBUILD ENV NODE_ENV $NODE_ENV
ONBUILD COPY package.json /usr/src/app/
ONBUILD RUN npm install && npm cache clean --force
ONBUILD COPY . /usr/src/app

CMD [ "npm", "start" ]
```

### Bonnes Pratiques

##### .dockerignore
Une bonne pratique consiste à créer un fichier .dockerignore à la racine du projet où se trouve le fichier Dockerfile. Ce fichier va lister les dossiers et fichiers à ne pas inclure dans le contexte de construction de l'image, comme par exemple un dossier **.git**. 

Cela permet d'acccélerer la phase de construction en n'utilisant que les fichiers nécessaires à la création de l'image, et éviter de copier par accident des fichiers non voulus dans l'image Docker avec l'instruction **COPY** par exemple

La notation de ce fichier .dockerignore est similaire à un fichier .gitignore

##### Nombre d'instructions et couches Docker (Layers)
Veuillez pendre note que chaque instructions dans un Dockerfile va générer une couche réutilisable. Plus vous ajoutez des instructions, plus la taille finale de l'image sera grande. Il s'agit donc de minimiser au maximum le nombre d'instructions pour limiter la taille d'une image, et chainer les commandes dans une instruction si possible en utilisant les caractères de chainage : &&

Nous verrons plus en détails l'influence des couches (layers) dans la partie **stockage**

#### Ordre des instructions
L'ordre des instructions est également important, si une instruction change entre deux constructions d'images, cela va invalider la suite des instructions dans le Dockerfile et ainsi nécessiter la reconstruction de l'image à partir de ce point.

Il est donc recommandé de placer les instructions qui changent le moins au début du fichier.

Exemple:
```
FROM node:7-alpine

WORKDIR /app

COPY . /app
RUN npm install

ENTRYPOINT ["./entrypoint.sh"]
CMD ["start"]
```

Dans l'exemple ci-dessus, le code changera souvent et nous ne voulons pas réinstaller les packages à chaque fois. Nous pouvons ainsi copier package.json avant le reste du code, installer des dépendances, puis ajouter d'autres fichiers. 

Appliquons cette amélioration au fichier Dockerfile:

```
FROM node:7-alpine

WORKDIR /app

COPY package.json /app
RUN npm install
COPY . /app

ENTRYPOINT ["./entrypoint.sh"]
CMD ["start"]
```

#### Les containers ne doivent lancer qu'un seul processus
Techniquement vous pouvez démarrer plusieurs processus dans un container (à l'aide de Supervisor par exemple) pour avoir une application frontend, une application backend et une base de données, mais cela est une très mauvaise pratique.

- Phase de construction d'images alongées
- Images trop volumineuse
- Logging plus complexe provenant d'applications multiples
- Scaling horizontal gaspillé

Cela revient a recréer une application monolitique.

#### Ajouter des instructions **LABEL**

Il existe une instruction **LABEL** pour ajouter des métadonnées à l'image, telles que des informations sur le responsable ou la description étendue. 

Les métadonnées sont parfois utilisées par des programmes externes
```
FROM node:7-alpine
LABEL maintainer "john.doe@unknown.com"
```

#### Ajouter une instruction **HEALTHCHECK**

Il est possible de démarrer un conteneur Docker avec l'option `--restart always` . Après un crash de conteneur, le Docker daemon essaiera de le redémarrer. C'est très utile si votre conteneur doit être opérationnel tout le temps. 

Mais que se passe-t-il si le conteneur est en cours d'exécution, mais n'est pas disponible (boucle infinie, configuration non valide, etc.)?. 

Avec l'instruction **HEALTHCHECK**, nous pouvons demander à Docker de vérifier périodiquement l'état de santé de notre conteneur. Il peut s'agir de n'importe quelle commande, renvoyant 0 code de sortie si tout va bien, et 1 dans les autres cas.

```
FROM node:7-alpine
...
HEALTHCHECK CMD curl --fail http://localhost:$APP_PORT || exit 1

ENTRYPOINT ["./entrypoint.sh"]
CMD ["start"]
```

### Multi Stage Builds

Permet d'avoir une image avec seulement les éléments nécessaires à l'exécution de l'application.

```
FROM gradle:jdk10 as builder

# Copy src
COPY . .
# Run Gradle build
RUN gradle build

FROM openjdk:10-jre-slim
EXPOSE 8080
COPY --from=builder /home/gradle/build/myapp.jar /opt/myapp.jar

CMD ["java", "-jar", "/opt/myapp.jar", "run"]
```
Il est également possible d'utiliser une image externe comme "stage" du build

## Prochaines étapes
Pour la suite, rendez-vous sur la page suivante [3.0 Intéractions avec les Images](images.md)