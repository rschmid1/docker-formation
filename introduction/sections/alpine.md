## 1.0 Lancement de votre premier container

Après avoir validé votre installation Docker nous allons pouvoir démarrer votre premier container basé sur [Alpine Linux](http://www.alpinelinux.org/) (distribution linux parmi les plus légères) sur votre système pour avoir un aperçu de la commande `docker run`.

Pour commencer, veuillez exécuter la commande suivante dans votre terminal:
```
$ docker pull alpine
```

La commande `pull` récupère l'**image** alpine  du **registre Docker** (Docker Registry) et la dépose sur votre système. Vous pouvez utiliser la commande `docker images` pour voir la liste de toutes les images présentes sur votre systeème.
```
$ docker images
REPOSITORY              TAG                 IMAGE ID            CREATED             VIRTUAL SIZE
alpine                  latest              196d12cf6ab1        7 weeks ago         4.41MB
hello-world             latest              4ab4c602aa5e        7 weeks ago         1.84kB
```

### 1.1 Docker Run
A présent nous allons pouvoir lancer un **container** Docker basé sur cette image. Pour cela vous allez utiliser la commande `docker run`.

```
$ docker run alpine ls -l
total 52
drwxr-xr-x    2 root     root          4096 Sep 11 20:23 bin
drwxr-xr-x    5 root     root           340 Nov  1 14:59 dev
drwxr-xr-x    1 root     root          4096 Nov  1 14:59 etc
drwxr-xr-x    2 root     root          4096 Sep 11 20:23 home
drwxr-xr-x    5 root     root          4096 Sep 11 20:23 lib
......
......
```
Que s'est-il passé ? Lorsque la commande `run` est exécutée,
1. Le client Docker contactes le Docker daemon
2. Le Docker daemon vérifie dans le stockage local si cette image (alpine dans notre cas) est déjà disponible localement, et si ce n'est pas le cas, télécharges l'image depuis le Docker Store. (Puisque nous avons exécuté la commande `docker pull alpine` avant, le téléchargement n'est pas nécessaire)
3. Le Docker daemon crée le container et lance une commande dans ce container.
4. Le Docker daemon affiche la sortie de la commande au client Docker

Lorsque vous lancer la commande `docker run alpine`, une commande a été fournie (`ls -l`), et donc Docker a exécuté cette commande et vous pouvez voir la liste des fichiers et dossiers du répértoire de travail du container (**WORKDIR**).

Essayons une autre commande.

```
$ docker run alpine echo "hello from alpine"
hello from alpine
```
Dans ce cas, le client Docker a lancé la commande `echo` dans le container alpine et le container se termine de suite. Comme vous avez pu le remarquer, tout s'est passé très vite. Imaginer faire de même avec une machine virtuelle, la démarrer, lancer la commande et la terminer. Maintenant vous savez pourquoi on dit que l'exécution de containers est très rapide!

Essayons une autre commande.
```
$ docker run alpine /bin/sh
```

Vous pouvez noter que rien ne se passe, en réalité tout s'est passé très vite, le container a démarré et s'est éteint instantanément. Ces shells interactifs vont se terminer après avoir exécuté toute commande scriptée, sauf si le container est lancé dans un terminal interactif - donc pour que le container ne se termine pas de suite en lancant cette commande vous devez lancer la commande suivante `docker run -it alpine /bin/bash` ou `docker run -it alpine /bin/sh` suivant le shell disponible dans le container. 

Notez qu'il n'est pas nécessaire de spécifier `/bin/bash` ou `/bin/sh` vous pouvez aussi utiliser la forme raccourcie:

 `docker run -it alpine sh` ou `docker run -it alpine sh`

Vous êtes à présent à l'intérieur du shell du container et vous pouvez essayer quelques commndes comme `ls -l`, `uname -a` et autres. 

Sortez du container en exécutant la commande `exit` ou en appuyant sur la combinaison de touche `CTRL+P` et ensuite `CTRL+Q`.

L'utilisation d'un terminal interactif est rendu possible grace aux paramètre `-i` et `-t` qui indiquent respectivement de lancer le container en mode interactif et de s'attacher au terminal du container (tty)

Vous pouvez ainsi exécuter plusieurs commandes à l'intérieur du container si nécessaire:
```
$ docker run -it alpine /bin/sh
/ # ls
bin    dev    etc    home   lib    media  mnt    proc   root   run    sbin   srv    sys    tmp    usr    var
/ # uname -a
Linux 4093ed8ebf6a 4.9.125-linuxkit #1 SMP Fri Sep 7 08:20:28 UTC 2018 x86_64 Linux
```

A présent nous pouvons essayer la commande `docker ps`. La commande `docker ps` va afficher les containers qui sont actuellement démarrés sur votre système.

```
$ docker ps
CONTAINER ID        IMAGE               COMMAND             CREATED             STATUS              PORTS               NAMES
```

Etant donné qu'aucun container n'est actuellement en exécution, la liste est vide. Par contre vous pouvez essayer d'ajouter l'option `-a` ou `--all` à la commande: `docker ps -a`

```
$ docker ps -a
CONTAINER ID        IMAGE               COMMAND                  CREATED             STATUS                      PORTS               NAMES
36171a5da744        alpine              "/bin/sh"                5 minutes ago       Exited (0) 2 minutes ago                        fervent_newton
a6a9d46d0b2f        alpine             "echo 'hello from alp"    6 minutes ago       Exited (0) 6 minutes ago                        lonely_kilby
ff0a5c3750b9        alpine             "ls -l"                   8 minutes ago       Exited (0) 8 minutes ago                        elated_ramanujan
c317d0a9e3d2        hello-world         "/hello"                 34 seconds ago      Exited (0) 12 minutes ago                       stupefied_mcclintock
```

Nous obtenons une liste de tous les containers que vous avez exécutés mais qui sont arrétés. Veuillez noter la colonne `STATUS` qui indique que ces containers ont été stoppés il y a quelques minutes.

Pour connaître d'avantages d'options sur la commande `run`, utilisez la commande `docker run --help` pour voir une liste de toutes les options supportées, et pour obtenir encore plus de détails ou exemples rendez-vous sur la page de [référence](https://docs.docker.com/engine/reference/run/) sur le site de Docker 

### 1.2 Terminologie
Dans cette dernière section, nous avons pu voir une multitude de termes spécifique à Docker qui peuvent peut-être vous amener de la confusion. 

Avant de continuer, clarifions quelques termes qui sont utilisé fréquemment dans l'ecosystème Docker.

- *Images* - Le système de fichier et configuration de votre application qui est utilisé pour créer des containers. Pour avoir des informations supplémentaires sur une image, vous pouvez lancer la commande `docker inspect alpine`. Dans la section ci-dessus, la commande `docker pull` a été utilisée pour télécharger l'image **alpine**. Lorsque la commande `docker run hello-world` a été éxecutée, il y a aussi eu un `docker pull` qui a été effectué en tâche de fond pour télécharger l'image **hello-world**.
- *Containers* - Exécuter des instances d'images Docker &mdash; les containers exécutent les applications. Un container inclut une application et toutes ses dépendances. Il partage le kernel avec les autres containers, et lance un processus isolé dans la partie ***user space*** sur le système d'exploitation de l'hôte. Un container est crée lorsque la commande `docker run` est exécuté, ce qui as été fait dans la section ci-dessus en utilisant l'image **alpine** que vous avez téléchargé. La liste des containers en exécution peut être affichée en utilisant la commande `docker ps`.
- *Docker daemon* - Le service en tâche de fond qui tourne sur la machine hôte qui gère la construction d'images, ainsi que l'exécution et la distribution de container Docker.
- *Docker client* - L'outil en ligne de commande qui permet à l'utilisateur d'intéragir avec le *Docker daemon*.
- *Docker Store* - Un [registre](https://store.docker.com/) d'images Docker, où vous pouvez trouver des images de confiance et prêt pour l'entreprise, des plugins, et les différentes éditions Docker.

## Prochaines étapes
Pour la suite, rendez-vous sur la page suivante [2.0 Création d'une image](dockerfile.md)