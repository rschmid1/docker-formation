# Nettoyage

Pour gérer la suppression des containers, des réseaux, des images et des volumes vous pouvez utiliser la commande: `docker system prune -- all | -f`

Cette commande va enlever :
- Tous les containers stoppés
- Tous les réseaux non utilisés par au moins un container
- Toutes les images dites « dangling » (non associées avec une image ou un container, comme les images crées dans les étapes intermédiaires)
- Tous les caches de build

En ajoutant `-a` ou `--all` : enlève toutes les images non utilisées pas seulement les images dites **« dangling »**

En ajoutant `-f` : on force la commande et on ne demande pas de confirmation

## Nettoyage des volumes
`docker system prune --volume`

En ajoutant `--volume` à la commande `system prune` il est possible d’enlever en plus de la commande initiale :
- Tous les volumes non utilisé par au moins un container, par défaut les volumes ne sont pas supprimés

Notez que dans un cluster, la command `docker system prune` nécessite son exécution sur chacun de nœuds.

## Prochaines étapes
Pour la suite, rendez-vous sur la page suivante [6.0 Résumé](summary.md)