
# Architecture et interactions Docker
![Docker Interactions architecture](architecture-docker-interactions.png)

# Architecture client Docker
![Docker client architecture](architecture-docker-client.png)

# Différences entre une VM et un container
![Classic VM Architecture](classic-vm-architecture.png) ![Containers Architecture](containers-architecture-on-hosts.png)