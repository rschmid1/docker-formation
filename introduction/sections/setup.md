## Installation

### Configurer votre machine
Le guide *getting started* sur le site de Docker vous permet de trouver des instruction détaillées pour installer Docker sur de multiples plateformes sur [Mac](https://docs.docker.com/docker-for-mac/), [Linux](https://docs.docker.com/engine/installation/linux/) et [Windows](https://docs.docker.com/docker-for-windows/).

*Si vous utiliser Docker for Windows* assurez vous d'avoir [partagé votre disque dur](https://docs.docker.com/docker-for-windows/#shared-drives).

*Toutes les commandse fonctionnent soit en bash ou Powershell sur Windows*

Une fois que vous avez installé Docker, vous pouvez la commande suivante pour valider votra installation :
```
docker run hello-world
Unable to find image 'hello-world:latest' locally
latest: Pulling from library/hello-world
d1725b59e92d: Pull complete 
Digest: sha256:0add3ace90ecb4adbf7777e9aacf18357296e799f81cabc9fde470971e499788
Status: Downloaded newer image for hello-world:latest

Hello from Docker!
This message shows that your installation appears to be working correctly.

To generate this message, Docker took the following steps:
 1. The Docker client contacted the Docker daemon.
 2. The Docker daemon pulled the "hello-world" image from the Docker Hub.
    (amd64)
 3. The Docker daemon created a new container from that image which runs the
    executable that produces the output you are currently reading.
 4. The Docker daemon streamed that output to the Docker client, which sent it
    to your terminal.

To try something more ambitious, you can run an Ubuntu container with:
 $ docker run -it ubuntu bash

Share images, automate workflows, and more with a free Docker ID:
 https://hub.docker.com/

For more examples and ideas, visit:
 https://docs.docker.com/get-started/
```
## Prochaines étapes
Pour la suite, rendez-vous sur la page suivante [1.0 Lancement de votre premier container](alpine.md)