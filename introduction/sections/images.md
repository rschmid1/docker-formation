## 3.0 Intéractions avec les Images

### Docker Pull

La commande `docker pull` permet de télécharger une image sur l'hôte

- Sans aucun tag, récupère la version taggée **latest**:

  Attention, utiliser le tag **latest** n'est pas recommandé, car il ne s'agit en réalité que d'un tag et n'assures en rien qu'il s'agit effectivement de la dernière version. De plus vous perdez le contrôle de vos images qui se basent sur une image **latest**, et mieux vaut utiliser une version fixe comme par exemple : ***tomcat:8.5.34-jre8-alpine***
  ```
  docker pull tomcat:8.5.34-jre8-alpine
  ```
- Avec l'option `-a` ou `--all` télécharges toutes les versions d'images (images taggées):
  ```
  docker pull -a tomcat
  ```
- Pour télécharger d'un registre non vérifié:

  Attention, cette option est à utiliser avec précaution et n'est à faire seulement si vous pouvez faire confiance au registre d'où provient l'image. Cette option peut être ajoutée de manière permanente dans le fichier **/etc/docker/daemon.json** pour l'utilisation d'un registre privé avec un certificat auto-signé par exemple.
  ```
  docker pull --disable-content-trust <image_name>
  ```

### Docker images

La commande `docker images` permet d'intéragir avec les images qui se trouvent sur l'hôte

- Lister les images sur l'hôte: 
  ```
  docker images or docker images -a or --all
  ```
- Filtrer le résultat des images Docker images en utilisant l'option `--filters`:
    ```
    docker images --filter "before=centos:6"
    docker images --filter "since=centos:6"
    ```
- Afficher l'identifiant dans sa globalité (pas de coupure):
  ```
  docker images --no-trunc
  ```
- Afficher seulement les identifiants des images avec l'option silence `-q`: 
  ```
  docker images -q
  ```

### Tag d'image

Les ***tags*** d'images ont un fonctionnment similaire aux tags **git** et branches, ils sont comme des marque-pages qui pointent sur l'identifiant d'une image. 

L'ajout d'un **tag** sur une image ne renomme pas l'image, cela ajoute un nouveau **tag**:

```
docker tag <image-existante> mon-image:v1
```

Pour enlever un tag sur une image:
```
docker rmi mon-image:v1
```

### Docker Push
La commande `docker push` permet de téléverser une image depuis l'hôte sur le Docker Hub (régistre Docker publique par défaut). 

Pour pouvoir exécuter cette commande avec le Docker Hub, il est nécessaire d'avoir un compte (gratuit) et de lancer la commande `docker login` au préalable pour s'authentifier sur le registre.

Ainsi, gràce à la commande `docker push` vous pouvez mettre à disposition pour tout le monde une image que vous avez construite. Dans une utilisation de Docker en entreprise, il est d'uage d'avoir un registre d'images Docker en interne ou sur le Cloud en mode privé.

```
docker tag tomcat:8.5.34-jre8-alpine sw/tomcat:v1
docker login -u myusername
docker push sw/tomcat:v1

```
### Addresses de registre Docker

Quand nous poussons une image Docker sur un registre, l'adresse du registre docker fait partie du nom d'image et de son tag.

Exemple: `nexus.local:8082/nginx`

```
REPOSITORY                  TAG                 IMAGE ID            CREATED             SIZE

alpine                      3.8                 196d12cf6ab1        7 weeks ago         4.41MB
tomcat                      8.5.34-jre8-alpine  a06816855871        7 weeks ago         107MB
sw/tomcat                   v1                  dbdf28895b67        3 hours ago         108MB
nexus.local:8082/nginx      latest              dbfc48660aeb        2 weeks ago         109MB
...
```

Concernant les images se trouvant sur DockerHub, dans l'exemple ci-dessus, l'addresse de l'image alpine officielle est en réalité: `index.docker.io/library/alpine`

Et l'image `sw/tomcat` est `ìndex.docker.io/sw/tomcat` ***sw*** étant un nom d'utilisateur sur le DockerHub


### Export et import d'images

#### Export
Pour exporter une image vous pouvez utilise la commande `docker export`
```
docker export <nom-image> > mon-image.tar
```
Cette commande va consolider toutes les couches et réduire la taille d'une image.

#### Import
La commande d'import d'image :
```
docker import docker import mon-image.tar localimport:cento6
```

Il est possible d'utiliser la commande `docker load` pour charger une image et utiliser le nom et **tag** initial utilisé pour cette image avant sa sauvegarde:
```
docker load < image_bckp.tar
# ou
docker load --input image_bckp.tar
```

Notez que l'importation d'image va supprimer tout l'historique d'image et transforme l'image en une seule couche, et il y aura un commentaire indiquant : ***Imported from -*** 

### Recherche d'images

Il est possible de rechercher des images avec le client Docker à l'aide de la commande `docker search` 


Pour utiliser des filtres multiples il faut utiliser une option `-f` ou `--filter` pour chaque filtre

Par exemple, avec un filtre indiquant plus de 50 étoiles et seulement des images officiels:
```
docker search --filter stars=50 --filter is-official=true apache
NAME                DESCRIPTION                                     STARS               OFFICIAL            AUTOMATED
tomcat              Apache Tomcat is an open source implementati…   2125                [OK]                
httpd               The Apache HTTP Server Project                  2113                [OK]                
cassandra           Apache Cassandra is an open-source distribut…   888                 [OK]                
maven               Apache Maven is a software project managemen…   734                 [OK]                
solr                Solr is the popular, blazing-fast, open sour…   605                 [OK]                
zookeeper           Apache ZooKeeper is an open-source server wh…   503                 [OK]                
tomee               Apache TomEE is an all-Apache Java EE certif…   58                  [OK]                
groovy              Apache Groovy is a multi-faceted language fo…   58                  [OK]       
```

### Historique d'une image

La commande `docker history` permet de lister toutes couches d'une image (pour voir les couches non tronquées vous pouvez utiliser l'option `--no-trunc`). 

Il est donc important de ne pas stocker de données sensibles (mot de passe, etc...) dans les Dockerfile car ils seront visible avec la commande `docker history`

```
docker history tomcat:8.5.34-jre8-alpine
IMAGE               CREATED             CREATED BY                                      SIZE                COMMENT
4ac473a3dd92        10 days ago         /bin/sh -c #(nop)  CMD ["catalina.sh" "run"]    0B                  
<missing>           10 days ago         /bin/sh -c #(nop)  EXPOSE 8080/tcp              0B                  
<missing>           10 days ago         /bin/sh -c set -e  && nativeLines="$(catalin…   0B                  
<missing>           10 days ago         /bin/sh -c set -eux;   apk add --no-cache --…   24.5MB              
<missing>           10 days ago         /bin/sh -c #(nop)  ENV TOMCAT_ASC_URLS=https…   0B                  
<missing>           10 days ago         /bin/sh -c #(nop)  ENV TOMCAT_TGZ_URLS=https…   0B                  
<missing>           10 days ago         /bin/sh -c #(nop)  ENV TOMCAT_SHA512=131dfe2…   0B                  
<missing>           10 days ago         /bin/sh -c #(nop)  ENV TOMCAT_VERSION=8.5.34    0B                  
<missing>           10 days ago         /bin/sh -c #(nop)  ENV TOMCAT_MAJOR=8           0B                  
<missing>           10 days ago         /bin/sh -c #(nop)  ENV GPG_KEYS=05AB33110949…   0B                  
<missing>           10 days ago         /bin/sh -c #(nop)  ENV LD_LIBRARY_PATH=/usr/…   0B                  
<missing>           10 days ago         /bin/sh -c #(nop)  ENV TOMCAT_NATIVE_LIBDIR=…   0B                  
<missing>           10 days ago         /bin/sh -c #(nop) WORKDIR /usr/local/tomcat     0B                  
<missing>           10 days ago         /bin/sh -c mkdir -p "$CATALINA_HOME"            0B                  
<missing>           10 days ago         /bin/sh -c #(nop)  ENV PATH=/usr/local/tomca…   0B                  
<missing>           10 days ago         /bin/sh -c #(nop)  ENV CATALINA_HOME=/usr/lo…   0B                  
<missing>           10 days ago         /bin/sh -c set -x  && apk add --no-cache   o…   78.6MB              
<missing>           10 days ago         /bin/sh -c #(nop)  ENV JAVA_ALPINE_VERSION=8…   0B                  
<missing>           10 days ago         /bin/sh -c #(nop)  ENV JAVA_VERSION=8u181       0B                  
<missing>           7 weeks ago         /bin/sh -c #(nop)  ENV PATH=/usr/local/sbin:…   0B                  
<missing>           7 weeks ago         /bin/sh -c #(nop)  ENV JAVA_HOME=/usr/lib/jv…   0B                  
<missing>           7 weeks ago         /bin/sh -c {   echo '#!/bin/sh';   echo 'set…   87B                 
<missing>           7 weeks ago         /bin/sh -c #(nop)  ENV LANG=C.UTF-8             0B                  
<missing>           7 weeks ago         /bin/sh -c #(nop)  CMD ["/bin/sh"]              0B                  
<missing>           7 weeks ago         /bin/sh -c #(nop) ADD file:25c10b1d1b41d46a1…   4.41MB

```
### Inspection d'images
La commande `docker image inspect` permet d'obtenir une multitude d'informations concernant une image comme par exemple les ports exposés, le répértoire de travail courant, sous forme d'un résultat en JSON par défaut.

```
docker image inspect tomcat:8.5.34-jre8-alpine
[
    {
        "Id": "sha256:4ac473a3dd922eecfc8e0dabd9f6c410871dfd913fea49bfed3fb99924839131",
        "RepoTags": [
            "tomcat:8.5.34-jre8-alpine"
        ],
        "RepoDigests": [
            "tomcat@sha256:27b25752202d714051d15227f91e819ff1ff79833d33216d7bfc927954b6ef64"
        ],
        "Parent": "",
        "Comment": "",
        "Created": "2018-10-24T23:13:16.655461238Z",
        "Container": "fce5737d060d505c945d5b4c10ffb527bf1518019b3c603102914d5885f20061",
        "ContainerConfig": {
            "Hostname": "fce5737d060d",
            "Domainname": "",
            "User": "",
            "AttachStdin": false,
            "AttachStdout": false,
            "AttachStderr": false,
...

```

Vous pouvez filtrer les informations obtenues par la commande `inspect` à l'aide de l'option `--format` et une notation de templating du language **Go**
```
docker image inspect tomcat:8.5.34-jre8-alpine --format '{{.ContainerConfig.WorkingDir}}'
/usr/local/tomcat
```
### Nettoyage des images

Il existe des commandes pour supprimer des images de l'hôte pour par exemple libérer de l'espace disque.

#### Supression d'une image

`docker image rmi <image-id>` 

#### Suppression d'images multiples:

Pour supprimer toutes les images dites « dangling » (non associées avec une image ou un container, comme les images crées dans les étapes intermédiaires) vous pouvez utiliser la commande `docker image prune`

En ajoutant l'option `-a` ou `--all` on enlève toutes les images non utilisées (non associées à un container) pas seulement les images dites « dangling »

Attention avec cette commande, si vous voulez garder des images en cache qui ne sont pas actuellement utilsées.

## Prochaines étapes
Pour la suite, rendez-vous sur la page suivante [4.0 Intéractions avec les Containers](containers.md)