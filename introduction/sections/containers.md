### 4.0 Intéractions avec les containers

Les containers sont l'instanciation d'une image.

Pour démarrer un container il suffit d'utiliser la commande `docker run`

```
docker run mon-image:v1
```

Cette commande va lancer le container en tâche frontale, si vous vous voulez l'éxécuter en tâche d'arrière-plan vous devez ajouter l'option `-d`

```
docker run -d mon-image:v1
```

## Exposition des ports

Lorsque vous voulez exposer le(s) port(s) d'un container vous pouvez indiquer lesquelles exposer à l'aide de l'option `-p`

Exemple:
```

# Lancement d'un container sans exposer de ports
$docker run -d --name mynginx nginx
6e99669c2780a5b93892c3259b0c14469c924765cf008088068ef68b73a555a2

docker ps -l
CONTAINER ID        IMAGE               COMMAND                  CREATED             STATUS              PORTS               NAMES
6e99669c2780        nginx               "nginx -g 'daemon of…"   3 seconds ago       Up 2 seconds        80/tcp              mynginx

# Lancement d'un container en exposant le port 80 du container vers le port 7777 sur l'hôte
$ docker run -d --name mynginx -p7777:80 nginx
5af1c81edea783a5e1df8f25b081a06646a04cfaacfaa12ff60fb6db5f511bc5

$ docker ps -l
CONTAINER ID        IMAGE               COMMAND                  CREATED             STATUS              PORTS                  NAMES
5af1c81edea7        nginx               "nginx -g 'daemon of…"   14 seconds ago      Up 13 seconds       0.0.0.0:7777->80/tcp   mynginx

# Commande curl depuis l'hôte en utilisant le port 7777 de l'hôte mappé sur le port 80 du container
$ curl localhost:7777
<!DOCTYPE html>
<html>
<head>
<title>Welcome to nginx!</title>
<style>
    body {
        width: 35em;
        margin: 0 auto;
        font-family: Tahoma, Verdana, Arial, sans-serif;
    }
</style>
</head>
<body>
<h1>Welcome to nginx!</h1>
<p>If you see this page, the nginx web server is successfully installed and
working. Further configuration is required.</p>

<p>For online documentation and support please refer to
<a href="http://nginx.org/">nginx.org</a>.<br/>
Commercial support is available at
<a href="http://nginx.com/">nginx.com</a>.</p>

<p><em>Thank you for using nginx.</em></p>
</body>
</html>

```
 
 ## Exposition automatique de ports
 Pour exposer automatiquement les ports exposés dans l'image (grace aux instructions `EXPOSE` présentes dans l'image issus du Dockerfile)
```
$docker run -d --name mynginxauto -P nginx
aac0a63d64df2b1aa4cb6bf5dce8b13fc826bd45964b3e36d3d9810991fb870c

# Notez le port 32768 qui a été automatiquement été attribué sur l'hôte pour mapper le port 80 du container
$ docker ps -l
CONTAINER ID        IMAGE               COMMAND                  CREATED             STATUS              PORTS                   NAMES
aac0a63d64df        nginx               "nginx -g 'daemon of…"   5 seconds ago       Up 3 seconds        0.0.0.0:32768->80/tcp   mynginxauto

# Il est possible également d'obtenir quel port est mappé sur l'hôte pour un container donné à l'aide de la commande donnée:
$ docker container port mynginxauto 80
0.0.0.0:32768

```

### Connexion à un container 

La commande `docker exec` permet de lancer une commande à l'intérieure d'un container, 
```
docker exec <container-id> ping google.com
```
Cette commande permet également de pouvoir de se connecter à l'intérieur du container en utilisant les options `-i` et `-t` et indiquant quel shell utiliser. Cela va créer un processus supplémentaire au processus principal du container. 
```
docker exec -it <container-id> sh ou bash
```

Sortir du container après cette commande ne termine pas le container.

## Logs des containers

Pour afficher les logs d'un container vous pouvez utiliser la commande `docker container logs`
```
docker logs <container-id>
...
05-Nov-2018 10:10:57.876 INFOS [localhost-startStop-1] org.apache.catalina.startup.HostConfig.deployDirectory Deployment of web application directory [/usr/local/tomcat/webapps/ROOT] has finished in [908] ms
05-Nov-2018 10:10:57.880 INFOS [main] org.apache.coyote.AbstractProtocol.start Starting ProtocolHandler ["http-nio-8080"]
05-Nov-2018 10:10:57.893 INFOS [main] org.apache.coyote.AbstractProtocol.start Starting ProtocolHandler ["ajp-nio-8009"]
05-Nov-2018 10:10:57.897 INFOS [main] org.apache.catalina.startup.Catalina.start Server startup in 1118 ms

```
Vous pouvez également utiliser l'option `--tail` et indiquer le nombre de lignes à afficher:
```
docker container logs --tail -10 <container-id> 
```
Pour suivre en direct les logs d'un container vous pouvez ajouter l'option `--follow`
```
# affiche dernière ligne et continue à afficher en temps réel
docker container logs --tail 1 --follow <container-id> 
```

Il existe plusieurs pilotes (drivers) pour traiter les logs d'un container. Par défaut, le pilote **json-driver** est utilsé et la commande `docker container logs` ne fonctionne que lorsque ce pilote est utilisé. 

Il est possible d'utiliser d'autres pilotes comme par exemple : 
syslog, journald,gelf, fluentd,awslogs,splunketwlogs,gcplogs,et logentries

Pour plus de détails concernant le fonctionnement des logs et les différents pilotes de logs disponible vous pouvez vous rendre sur la page de [référence](https://docs.docker.com/config/containers/logging/) sur le site de Docker.

## Stopper un container

La commande `docker stop <container-id>` va envoyer le signal `TERM` au container, et ainsi permettre au container de s'arrêter proprement. 

Si le container ne réponds pas  ou ne supporte pas le signal `TERM` Docker envoie le signal `KILL` après 10 secondes pour l'arrêter de manière abrupte.

Vous pouvez donc également exécuter la commande `docker kill <container-id>`

## Lister les containers
Pour identifier quels sont les containers qui sont actuellement exécutés sur un hôte Docker vous pouvez utiliser la commande `docker ps` ou `docker ps -a` pour lister tous les containers indépendamment de leur état (created, restarting, running, removing, paused, exited, or dead) comme nous l'avons déjà vu précédemment.

Vous pouvez également utilisez l'option `-l` qui permet d'afficher le dernier container démarré: `docker ps -l`, ou juste son identifiant avec l'option silencieuse `-q` : `docker ps -lq`

#### Docker commit
Il est possible de créer une nouvelle image issue des changements qu'il y a eu dans un container à l'aide de la commande `docker commit`:
```
docker commit <container-id>  image-test:v1
```
Il peut être utile de valider les modifications de fichier ou les paramètres d’un conteneur dans une nouvelle image. Cela vous permet de déboguer un conteneur en exécutant un shell interactif ou d'exporter un jeu de données de travail vers un autre serveur. Cependant, en règle générale, il est préférable d'utiliser des Dockerfiles pour gérer vos images de manière documentée et maintenable.

Prenez note que cette opération de `commit` n'inclut aucune donnée contenue dans des volumes montés à l'intérieur du conteneur.

Par défaut, le conteneur en cours de validation et ses processus seront suspendus pendant la validation de l'image. Cela réduit le risque de corruption des données pendant le processus de création du commit. Si ce comportement n'est pas souhaité, définissez l'option `--pause` sur `false`

Vous pouvez également faire une opération de `commit` sur un container en changeant des configurations d'environnement ou des instructions **CMD** ou **ENTRYPOINT** par exemple à l'aide de l'option `--change`:
```
$ docker inspect -f "{{ .Config.Env }}" <container-id>
[HOME=/ PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin]
$ docker commit --change "ENV DEBUG true" <container-id> image-test:v1
$ docker inspect -f "{{ .Config.Env }}" <nouvel-id-image>
[HOME=/ PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin DEBUG=true]
```

#### Docker diff

Liste les fichiers et les répertoires modifiés dans le système de fichiers d'un conteneur depuis la création de celui-ci. 

Il existe trois types de changement visible dans l'affichage du résultat de la commande:
- A : Un fichier ou un dossier a été ajouté
- D : Un fichier ou un dossier a été supprimé
- C : Un fichier ou un dossier a changé

```
docker diff 5423fa16d095
C /usr
C /usr/local
C /usr/local/tomcat
C /usr/local/tomcat/conf
C /usr/local/tomcat/conf/server.xml
C /usr/local/tomcat/conf/logging.properties
A /usr/local/tomcat/conf/Catalina
A /usr/local/tomcat/conf/Catalina/localhost
C /usr/local/tomcat/bin
A /usr/local/tomcat/bin/setenv.sh
A /usr/local/tomcat/bin/setenv.she
C /usr/local/tomcat/work
A /usr/local/tomcat/work/Catalina
A /usr/local/tomcat/work/Catalina/localhost
A /usr/local/tomcat/work/Catalina/localhost/ROOT
C /tmp
C /tmp/hsperfdata_root
A /tmp/hsperfdata_root/1
```

### Nettoyage des containers

#### Suppression d'un container

La suppression d'un container va également supprimer les données éphémère qui se trouvent dans le container, mais ne va pas supprimer les éventuels volumes associés.

`docker container rm <container-id>`

#### Suppression de tous les containers stoppés:
`docker container prune`

Attention à l'utilisation de cette commande si vous avez besoin de redémarrer un container ultérieurement

## Prochaines étapes
Pour la suite, rendez-vous sur la page suivante [5.0 Nettoyage](cleanup.md)