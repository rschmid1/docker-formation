
# Services

La syntaxe est assez proche de la commande `docker run` mais avec quelques options en plus.
```
$ docker service create \
  --name <SERVICE-NAME> \
  --publish published=<PUBLISHED-PORT>,target=<CONTAINER-PORT> \
  <IMAGE>

  $ docker service create \
  --name my-web \
  --publish published=8080,target=80 \
  --replicas 2 \
  nginx:mainline

```

### Commençons par la création d'un service nginx avec un seule réplica
```
$ docker service create --replicas 1 --name web -p 80:8080 nginx:mainline
7b2osbdqc3sg84mixgzxip1fk
overall progress: 1 out of 1 tasks 
1/1: running   [==================================================>] 
```

### Puis multiplions les replicas par 5

``` 
$ docker service scale web=5
web scaled to 5
overall progress: 5 out of 5 tasks 
1/5: running   [==================================================>] 
2/5: running   [==================================================>] 
3/5: running   [==================================================>] 
4/5: running   [==================================================>] 
5/5: running   [==================================================>] 
verify: Service converged
```

### Vérifions la configuration du service **web** en listant tous les service du cluster
```
$ docker service ls
ID            NAME  REPLICAS  IMAGE                 COMMAND
cwrfie1hxn8g  web   5/5       nginx:mainline
```
l’état désiré du service web (5 replicas) est bien égal à son état actuel.

### Vérifions sa répartition sur les 3 nœuds (1 master et 2 workers)
```
$ docker service ps web
ID                         NAME   IMAGE           NODE    DESIRED STATE  CURRENT STATE           ERROR
7b2osbdqc3sg84mixgzxip1fk  web.1  nginx:mainline  node1   Running        Running 5 minutes ago
cwpw2g0mw5gqnds0mvdem5gyx  web.2  nginx:mainline  master  Running        Running 26 seconds ago
2y4mhbc6liaohkls5io76vwbu  web.3  nginx:mainline  node2   Running        Running 26 seconds ago
f01kh9kn8pprj528mnl3xnzj1  web.4  nginx:mainline  node2   Running        Running 26 seconds ago
avn8ukb1jt9zd7ct462h07e0l  web.5  nginx:mainline  node1   Running        Running 26 seconds ago
```

Nous pouvons voir que le cluster **Swarm** essaye de répartir "au mieux" les 5 replicas du service **web**.

### Pour avoir plus de détails sur le service **web** nous pouvons faire une "inspection"
```
$ docker service inspect
```

## Différence entre les services globaux et les services répliqués
La différence est très simple à comprendre :

Lors de l'utilisation des services distribués vous spécifiez exactement le nombre de réplicas que vous souhaitez.

**Swarm** essaye de gérer au mieux votre demande sur tous les nœuds disponibles.

Quant aux services globaux, c'est bien plus simple : un seul réplica sur chaque nœud

Pour configurer un service **global** il faut ajouter l'option `--mode global`, exemple :
```
$ docker service create \
  --mode global \
  --publish mode=host,target=80,published=8080 \
  --name=nginx \
  nginx:mainline
```

![Replicated vs Global](replicated-vs-global.png)

## Comment supprimer un service ?
```
$ docker service rm web
web

$ docker service ps web
ID                         NAME   IMAGE                 NODE    DESIRED STATE  CURRENT STATE           ERROR
```

## Prochaines étapes
Pour la suite, rendez-vous sur la page suivante [5.0 Mise à jour d'un service (Rolling update)](rolling-update.md)

