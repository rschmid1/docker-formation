# Stack

Nous avons vu l'utilisation de la commande `docker service create` dans une section précédente, passons à présent à l'utilisation d'une **Stack**.

Une **Stack** qu'est-ce que c'est ?

Ce n'est ni plus ni moins qu'une définition d'un ensemble de services décrit dans un type de fichier que vous connaissez déjà, à savoir un fichier au format **Docker Compose**.

Cela simplifie le déploiement d'une **Stack** et évite de lancer une multitude de commandes `docker service create`. 

Cela ajoute également une notion de documentation implicite de la **Stack**.

Un nouvel élement apparait dans le fichier docker-compose.yml: **deploy**.

```
version: "3"
services:
  web:
    image: nginx:mainline
    deploy:
      replicas: 5
      restart_policy:
        condition: on-failure
      resources:
        limits:
          cpus: "0.1"
          memory: 50M
    ports:
      - "9600:80"
    networks:
      - webnet
  visualizer:
    image: dockersamples/visualizer:stable
    ports:
      - "9500:8080"
    volumes:
      - "/var/run/docker.sock:/var/run/docker.sock"
    deploy:
      placement:
        constraints: [node.role == manager]
    networks:
      - webnet
networks:
  webnet:
```

L'élément **deploy** permet d'indiquer la manière de déployer un service dans un cluster **Swarm**, et de spécifier la façon de déployer des services et leurs contraintes.

Cette instruction est ignorée lors de l'utilisation de l'outil **Docker Compose**  (par exemple : `docker-compose up`), il n'est reconnu seulement lorsque l'on utilise la commande `docker stack [COMMAND]`.

Observons les commandes disponibles pour la commande `docker stack`:
```
$ docker stack --help

Usage:	docker stack [OPTIONS] COMMAND

Manage Docker stacks

Options:
      --orchestrator string   Orchestrator to use (swarm|kubernetes|all)

Commands:
  deploy      Deploy a new stack or update an existing stack
  ls          List stacks
  ps          List the tasks in the stack
  rm          Remove one or more stacks
  services    List the services in the stack

```

Nous pouvons ainsi déployer ou enlever une **Stack**, lister les tâches d'une **Stack** en exécution ainsi que les services déclarés dans la stack.

Nous allons à présent utiliser le fichier `docker-compose.yml` décrit ci-dessus pour déployer notre première **Stack** à l'aide de la commande `docker stack`:

Pour démarrer cette **Stack** veuillez exécuter la commande suivante:
```
$ docker stack deploy --compose-file=docker-compose.yml my_nginx_stack
```

Rendez-vous ensuite sur un des noeuds de votre **Swarm** sur le port 9500 pour afficher le visualizer.

Pour l'exercice, vous pouvez mettre à jour le tag de l'image nginx (1.15 par exemple), et relancer la même commmande pour mettre à jour la **Stack**:
```
$ docker stack deploy --compose-file=docker-compose.yml my_nginx_stack
```

Comme nous l'avons déjà vu dans une section précédente, la **Stack** se met à jour de manière progressive (rolling update).

Vous pouvez suivre en direct la mise à jour, soit depuis l'interface web du visualiser, soit en faisant les commandes suivantes:
```
# récupérez le nom du service
$ docker service ls

# puis affcher les processus de la stack
$ docker service ps my_nginx_stack_web

# et pour obtenir plus de détails
# docker service inspect  my_nginx_stack_web

# Observer les éléments Spec{} et PreviousSpec{}
```

Notez que les instructions de `build` éventuels dans le fichier docker-compose.yml ne sont pas supportés. La commande `docker stack` n'accepte que des images déjà construites.

## Exercice supplémentaire
Déployer la stack **example-voting-app** disponible à l'addresse suivante: https://github.com/dockersamples/example-voting-app

Créer un répértoire sur votre instance Docker:
```
$ mkdir /opt/voting-app
```

Créer un fichier `docker-stack.yml` à l'intérieur du dossier : `/opt/voting-app` avec le contenu du fichier [docker-stack.yml](https://github.com/dockersamples/example-voting-app/blob/master/docker-stack.yml) présent dans  le repository git: 
```
$ curl https://raw.githubusercontent.com/dockersamples/example-voting-app/master/docker-stack.yml > docker-stack.yml
```

Puis exécuter la commande ci-dessous pour lancer la **Stack** de vote:
```
docker stack deploy --compose-file=docker-stack.yml voting_stack
```

Vous pouvez ensuite vous rendre sur la page de vote sur le port 5000 d'un des noeuds de votre cluster **Swarm** (`http://<node-ip>:5000`), et observer le résultat des votes sur le port 5001 (`http://<node-ip>:5001`)

Notez l'identifiant du container id sur la page `http://<node-ip>:5000` et de sa mise à jour lors de chaque rafraichissment de la page mettant en évidence le load balancer natif du cluster **Swarm** (**Ingress Routing Mesh**)

Vous pouvez lancer un update de la **Stack** en changeant le tag des images de `:before` à `:after`

```
dockersamples/examplevotingapp_result:before -> dockersamples/examplevotingapp_result:after
dockersamples/examplevotingapp_vote:before -> dockersamples/examplevotingapp_vote:after

docker stack deploy --compose-file=docker-stack.yml voting_stack
```

Rendez-vous sur l'écran des votes (`http://<node-ip>:5000`) et résultat des votes (`http://<node-ip>:5001`) pour observer la mise à jour appliquée.

## Prochaines étapes
Pour la suite, rendez-vous sur la page suivante [7.0 Docker Configs et Docker Secrets](swarm-config-secrets.md)