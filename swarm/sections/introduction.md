# Introduction

## Difference entre le lancement d'un Container et le lancement d'un Service

Les conteneurs sont flexibles, portables, granulaires et apportent une abstraction pour tirer le meilleur parti de nos environnements et déploiements, mais ils deviennent rapidement "limités".

Il faut un moyen plus simple de déployer des configurations complexes dans des implémentations hautement disponibles et facilement évolutives. Cela nécessite le développement de logiciels de gestion et de contrôle de cluster comme Swarm ou Kubernetes

Les services sont la solution pour gérer les conteneurs déployés dans des implémentations de cluster hautement disponibles et facilement évolutives.

Un service est quelque chose qui est consommé dans le contexte d'une application plus grande (qui peut inclure d'autres services Docker dans sa composition)

Considérant que les conteneurs sont limités à l'hôte unique sur lequel ils sont démarrés; les services sont des conteneurs qui vivent sur un nombre évolutif de "travailleurs" (**worker**) dans des systèmes de cluster.

Docker Swarm gère l'accès à ce service et la disponibilité de celui-ci sur tous les nœuds de travail (**worker nodes**), éliminant ainsi les problèmes de routage et d'accès à des conteneurs individuels.
Les services scalables vous permettent de contrôler de manière granulaire votre processeur, votre mémoire, votre disque, votre réseau, etc.

**Containers**
- Encapsuler une application ou une fonction
- Exécuter sur un seul hôte à la fois
- Exiger des étapes manuelles pour exposer les fonctionnalités en dehors du système hôte (ports, réseau et / ou volumes)
- Nécessite une configuration plus complexe pour utiliser plusieurs instances (proxies par exemple)
- Pas hautement disponible
- Pas facilement évolutif

**Services**:
- Encapsuler une application ou une fonction
- Peut fonctionner sur "1 à N" nœuds à tout moment
- La fonctionnalité est facilement accessible à l'aide de fonctionnalités telles que le "**routing mesh**" en dehors des nœuds de travail (**worker nodes**)
- Plusieurs instances sont configurées pour être lancées en une seule commande
- Des clusters hautement disponibles sont disponibles
- Facilement scalable (à la hausse ou à la baisse selon les besoins)

Les services nous apportent ainsi beaucoup plus de flexibilité dans l'utilisation des conteneurs dans nos environnements et nous permettent de gérer des environnements hautement disponibles et scalables.

Un cluster **Swarm** est composé de plusieurs hôtes Docker qui fonctionnent en mode **Swarm** et agissent en tant que gestionnaires (**manager**) pour gérer les membres et la délégation et en tant que travailleurs (**worker**) (qui gèrent des services swarm). 

Un hôte Docker donné peut être un **manager**, un **worker** ou exécuter les deux rôles. Lorsque vous créez un service, vous définissez son état optimal (nombre de replicas, ressources réseau et de stockage disponibles, ports que le service expose au monde extérieur, etc.). 

Docker travaille pour maintenir cet état souhaité. Par exemple, si un noeud **worker** devient indisponible, Docker planifie les tâches de ce noeud sur d’autres noeuds. Une tâche est un conteneur en cours d’exécution faisant partie d’un service **Swarm** et géré par un **manager** **Swarm**, par opposition à un conteneur autonome.

L'un des principaux avantages des services **Swarm** par rapport aux conteneurs autonomes est que vous pouvez modifier la configuration d'un service, y compris les réseaux et les volumes auxquels il est connecté, sans qu'il soit nécessaire de redémarrer manuellement le service. Docker met à jour la configuration, arrête les tâches de service avec la configuration obsolète et en crée de nouvelles correspondant à la configuration souhaitée.

![Docker Swarm Architecture](docker-swarm-architecture.jpg)

## Consensus Raft dans le mode **Swarm**

Lorsque le moteur Docker est exécuté en mode **Swarm**, les nœuds **manager** implémentent l'algorithme de consensus nommé **Raft** pour gérer l'état du cluster global.

Le mode **Swarm** de Docker utilise un algorithme de consensus pour s’assurer que tous les nœuds **manager** chargés de la gestion et de la planification des tâches dans le cluster stockent le même état cohérent.

Avoir le même état cohérent sur le cluster signifie qu'en cas de panne, tout noeud **manager** peut récupérer les tâches et restaurer les services dans un état stable. 

Par exemple, si le **manager leader** responsable chargé de la planification des tâches du cluster meurt de manière inattendue, tout autre **manager** peut choisir la tâche de planification et de rééquilibrage des tâches pour correspondre à l'état souhaité.

Les systèmes utilisant des algorithmes de consensus pour répliquer les journaux dans un système distribué nécessitent une attention particulière. Ils veillent à ce que l'état du cluster reste cohérent en présence de pannes en demandant à la majorité des noeuds de se mettre d'accord sur les valeurs.

Le consensus **Raft** tolère jusqu'à (N-1) / 2 échecs et requiert une majorité ou un quorum de (N / 2) +1 membres pour s'accorder sur les valeurs proposées au cluster. Cela signifie que dans un cluster de 5 gestionnaires exécutant Raft, si 3 nœuds ne sont pas disponibles, le système ne peut plus traiter de demandes de planification de tâches supplémentaires. Les tâches existantes continuent à s'exécuter, mais le planificateur ne peut pas rééquilibrer les tâches pour faire face aux échecs si l'ensemble de gestionnaires n'est pas sain.

## Prochaines étapes
Pour la suite, rendez-vous sur la page suivante [2.0 Initialisation d'un cluster **Swarm**](swarm-init.md)