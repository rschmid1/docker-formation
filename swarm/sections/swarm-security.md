# Sécurité dans le **Swarm**

## Encryption du trafic sur un réseau **overlay**
Tout le trafic de gestion de service swarm est crypté par défaut, à l'aide de l'algorithme AES en mode GCM. Les nœuds **Manager** du **Swarm** font pivoter la clé utilisée pour chiffrer les données issus du protocole **gossip** toutes les 12 heures.

Le manager ou le leader dans le cas ou nous avons plusieurs managers sera considéré comme "le root CA".

![Swarm TLS Communications](TLS_Encryption.png)

Pour chiffrer également les données d'application, ajoutez `--opt encrypted` lors de la création du réseau **overlay**. Cela active le cryptage IPSEC au niveau du vxlan. Ce chiffrement entraîne une pénalité non négligeable en termes de performances. Vous devez donc tester cette option avant de l'utiliser en production.

Lorsque vous activez le cryptage **overlay**, Docker crée des tunnels IPSEC entre tous les nœuds où des tâches sont planifiées pour les services liés au réseau **overlay**. Ces tunnels utilisent également l'algorithme AES en mode GCM et les nœuds de gestion font automatiquement pivoter les clés toutes les 12 heures.

Si vous suspectez que les certificats de un ou plusieurs de vos noeuds ont été compromis vous pouvez les révoquer très simplement, et ainsi en produire de nouveaux pour sécuriser l'ensemble des noeuds:

Effectuez la rotation d'abord sur les noeuds **manager**:
```
$ docker swarm join-token --rotate manager
Successfully rotated manager join token.
 
To add a manager to this swarm, run the following command:
 
    docker swarm join --token SWMTKN-1-27ntq8roo5zoxfpqrjddvl85byjuln9tm2xj5w7fz3ymodi645-0wvx82se9fc72q2vhesuyvke3 192.168.99.100:2377
```

Effectuez ensuite la rotation sur les noeuds **worker**:
```
docker swarm join-token --rotate worker
Successfully rotated worker join token.
 
To add a worker to this swarm, run the following command:
 
    docker swarm join --token SWMTKN-1-27ntq8roo5zoxfpqrjddvl85byjuln9tm2xj5w7fz3ymodi645-496wk05wdqcio1szrtd1dhi1a 192.168.99.100:2377
```

### Swarm lock

Il faut  savoir qu'une restauration d'un vieux nœud manager (avec les certificats valides) peut mettre en péril la sécurité de votre serveur.

Le nouveau nœud aura accès à la base de données utilisée par le protocole **Raft** et aura donc accès à tout votre cluster.

Pour votre prémunir d'une telle situation, Docker vous permet de vérouiller votre cluster pendant son fonctionnement:

```
docker swarm update --autolock=true
 
Swarm updated.
To unlock a swarm manager after it restarts, run the `docker swarm unlock`
command and provide the following key:
 
    SWMKEY-1-+MrE8NgAyKj5r3NcR4FiQMdgu+7W72urH0EZeSmP/0Y
 
Please remember to store this key in a password manager, since without it you
will not be able to restart the manager.
```

Redémarrez à présent un noeud manager pour tester le vérouillage:
```
service docker restart
```

Tentez ensuite une commande Swarm:
```
$ docker service ls
 
Error response from daemon: Swarm is encrypted and needs to be unlocked before it can be used. Use "docker swarm unlock" to unlock it.
```

Pour débloquer la Swarm:
```
$ docker swarm unlock
 
Please enter unlock key:
```
Après avoir fournit la précédente clé générée, vous allez à présent pouvoir effectuer toutes les commandes dans le cluster.

## Prochaines étapes
Pour la suite, rendez-vous sur la page suivante [4.0 Déploiement d'un service](services.md)