#!/bin/sh

docker pull redis:alpine
docker pull postgres:9.4
docker pull dockersamples/examplevotingapp_vote:before
docker pull dockersamples/examplevotingapp_result:before
docker pull dockersamples/examplevotingapp_worker
docker pull dockersamples/visualizer:stable

docker stack deploy --compose-file=docker-stack.yml voting_stack
