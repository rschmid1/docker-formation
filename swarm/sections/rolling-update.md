
# Mise à jour des services (Rolling Update)
Imaginons que nous avons développé un microservice en version 1 et que nous avons lancé 12 replicas sur 6 nœuds (mgr1→3 , wrk1→3).

Après un moment en production, nous souhaitons mettre à jour ce microservice en version 2 sans interruption de service.

Pour cela nous allons utiliser la fonctionnalité Rolling Update de Docker **Swarm** :


- Création du service initial ms-svc en version 1
- Le port 80 est exposé sur tous les noeuds et un réseau smart_ms est dédié au service.
- Le service sur le port 80 sera joinable de partout car le service tourne en mode ingress (les noeuds sans replicas pourront "rerouter" les demandes au service ms-svc sur les bons noeuds).
- Le nom du service est **ms-svc** basé sur la version 1 de l'image microservice


```
docker service create \
  --name ms-svc \
  --replicas 12
  --publish 80:80 \
  --network smart_ms
  smartwave/microservice:v1
 
 
$ docker service ls
ID NAME REPLICAS IMAGE
dhbtgvqrg2q4 ms-svc 12/12 smartwave/microservices:v1
```
Comme vous pouvez le constater le nombre de réplicas est 12.

Détaillons à présent la répartition avec la commande `docker service ps`:
``` 
$ docker service ps ms-svc
ID NAME IMAGE NODE DESIRED CURRENT STATE
0v...7e5 ms-svc.1 smartwave/...:v1 wrk3 Running Running 1 min
bh...wa0 ms-svc.2 smartwave/...:v1 wrk2 Running Running 1 min
23...u97 ms-svc.3 smartwave/...:v1 wrk2 Running Running 1 min
82...5y1 ms-svc.4 smartwave/...:v1 mgr2 Running Running 1 min
c3...gny ms-svc.5 smartwave/...:v1 wrk3 Running Running 1 min
e6...3u0 ms-svc.6 smartwave/...:v1 wrk1 Running Running 1 min
78...r7z ms-svc.7 smartwave/...:v1 wrk1 Running Running 1 min
2m...kdz ms-svc.8 smartwave/...:v1 mgr3 Running Running 1 min
b9...k7w ms-svc.9 smartwave/...:v1 mgr3 Running Running 1 min
ag...v16 ms-svc.10 smartwave/...:v1 mgr2 Running Running 1 min
e6...dfk ms-svc.11 smartwave/...:v1 mgr1 Running Running 1 min
e2...k1j ms-svc.12 smartwave/...:v1 mgr1 Running Running 1 min
```

Nous pouvons constater que les containers sont répartis sur tous les noeuds possibles.

Nous allons maintenant mettre à jour notre service **ms-svc** avec l'image `smartwave/microservices` version 2:

```
 docker service update \
--image smartwave/microservices:v2 \
--update-parallelism 2 \
--update-delay 20s ms-svc
```

Comme vous pouvez le constater, la mise à jour des replicas doit se faire 2 par 2 et avec 20 secondes d'intervalles entre les mises à jour

Regardons l'opération en direct avec la commande:  `docker service ps`

```
$ docker service ps ms-svc
ID NAME IMAGE NODE DESIRED CURRENT STATE
7z...nys ms-svc.1 smartw...v2 mgr2 Running 14 secs
0v...7e5 \_ms-svc.1 smartw...v1 wrk3 Shutdown 14 secs
bh...wa0 ms-svc.2 smartw...v1 wrk2 Running 1 min
e3...gr2 ms-svc.3 smartw...v2 wrk2 Running 13 secs
23...u97 \_ms-svc.3 smartw...v1 wrk2 Shutdown 13 secs
82...5y1 ms-svc.4 smartw...v1 mgr2 Running 1 min
c3...gny ms-svc.5 smartw...v1 wrk3 Running 1 min
e6...3u0 ms-svc.6 smartw...v1 wrk1 Running 1 min
78...r7z ms-svc.7 smartw...v1 wrk1 Running 1 min
2m...kdz ms-svc.8 smartw...v1 mgr3 Running 1 min
b9...k7w ms-svc.9 smartw...v1 mgr3 Running 1 min
ag...v16 ms-svc.10 smartw...v1 mgr2 Running 1 min
e6...dfk ms-svc.11 smartw...v1 mgr1 Running 1 min
e2...k1j ms-svc.12 smartw...v1 mgr1 Running 1 min
```

Effectivement 2 réplicas en v2 viennent d'être lancés sur le mgr2 et wkr2 tandis que que 2 autres en v1 sur les mêmes noeuds viennent de s'arrêter (status "shutdown").

La migration de v1 vers v2 se déroule donc correctement.


## Rollback

Il est possible de revenir en arrière avec la commmande `docker service rollback`

En effectuant cette commande, Docker va appliquer la spécification en place avant le spécification actuelle (image source, placement, etc...)

Pour consulter les spécifications d'un service vous pouvez exécuter la commande `docker service inspect <nom-du-service` et identifier l'élément **Spec** ou en filtrant avec `docker service inspect web -f "{{json .Spec}}"`. 

```
...
"Spec": {
  "Name": "web",
  "Labels": {},
  "TaskTemplate": {
      "ContainerSpec": {
          "Image": "nginx:1.15@sha256:d59a1aa7866258751a261bae525a1842c7ff0662d4f34a355d5f36826abc0341",
          "Init": false,
          "StopGracePeriod": 10000000000,
          "DNSConfig": {},
          "Isolation": "default"
      },
      "Resources": {
          "Limits": {},
          "Reservations": {}
      },
      "RestartPolicy": {
          "Condition": "any",
          "Delay": 5000000000,
          "MaxAttempts": 0
      },
      "Placement": {
        ...
      }
  }
}
...
```

Lors d'une opération de mise à jour d'un service, un nouvel élément va apparaitre : **PreviousSpec**. C'est avec le contenu de **PreviousSpec** qu'il pourra appliquer un éventuel rollback.

```
...
"PreviousSpec": {
  "Name": "web",
  "Labels": {},
  "TaskTemplate": {
      "ContainerSpec": {
          "Image": "nginx:mainline@sha256:d59a1aa7866258751a261bae525a1842c7ff0662d4f34a355d5f36826abc0341",
          "Init": false,
          "DNSConfig": {},
          "Isolation": "default"
      },
      "Resources": {
          "Limits": {},
          "Reservations": {}
      },
      "Placement": {
        ...
      }
  }
}
...
```
## Prochaines étapes
Pour la suite, rendez-vous sur la page suivante [6.0 Déploiement d'une stack](stack.md)