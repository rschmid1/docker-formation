# Overlay Network

Le pilote réseau **overlay** crée un réseau distribué entre plusieurs hôtes de Docker daemon. Ce réseau repose sur (superpose) les réseaux spécifiques à l’hôte, ce qui permet aux conteneurs qui lui sont connectés (y compris les conteneurs de service Swarm) de communiquer en toute sécurité. Docker gère de manière transparente le routage de chaque paquet vers et depuis le bon hôte Docker daemon et le conteneur de destination correct.

Lorsque vous initialisez un **Swarm** ou joignez un hôte Docker à un **Swarm** existant, deux nouveaux réseaux sont créés sur cet hôte Docker:

- un réseau **overlay** appelé **ingress**, qui gère le contrôle et le trafic de données lié aux services Swarm. Lorsque vous créez un service **Swarm** et que vous ne le connectez pas à un réseau **overlay** défini par l'utilisateur, il se connecte au réseau **ingress** par défaut.
- un réseau **bridge** appelé **docker_gwbridge**, qui connecte le Docker daemon individuel aux autres deamon participant aux **Swarm**.

Vous pouvez créer des réseaux **overlay** définis par l'utilisateur à l'aide de la commande `docker network create`, de la même manière que vous pouvez créer des réseaux **bridge** définis par l'utilisateur. Les services ou les conteneurs peuvent être connectés à plusieurs réseaux à la fois. Les services ou les conteneurs ne peuvent communiquer que sur les réseaux auxquels ils sont connectés.

**Load Balancing**

Le **manager Swarm** utilise l'équilibrage de charge en entrée (**ingress load balancing**) pour exposer les services que vous souhaitez rendre disponibles en externe au **Swarm**. Le **manager** **Swarm** peut attribuer automatiquement au service un port publié (`PublishedPort`) ou vous pouvez configurer un port publié (`PublishedPort`) pour le service. Vous pouvez spécifier n'importe quel port non utilisé. Si vous ne spécifiez pas de port, le **manager** **Swarm** affecte au service un port dans la plage 30000-32767.

Les composants externes, tels que les équilibreurs de charge dans le cloud (**cloud load balancer**), peuvent accéder au service sur le port publié (`PublishedPort`) de n'importe quel nœud du cluster, que le nœud exécute ou non la tâche pour le service. Tous les nœuds du **Swarm** routent les connexions entrantes (**ingress connections**) vers une instance de tâche en cours d’exécution.

Le mode **Swarm** comporte un composant DNS interne qui attribue automatiquement à chaque service du **Swarm** une entrée DNS. Le **manager** **Swarm** utilise un équilibrage de charge interne pour répartir les demandes entre les services du cluster en fonction du nom DNS du service.

### Ingress Routing Mesh
![Ingess routing mesh](ingress-routing-mesh.png)

### Ingress Load Balancer
![Ingess load balancer](ingress-lb.png)

## Contourner le **routing mesh** pour un service Swarm
Par défaut, les services **Swarm** qui publient des ports utilisent le **routing mesh**. Lorsque vous vous connectez à un port publié sur un nœud **Swarm** (qu'il exécute un service donné ou non), vous êtes redirigé de manière transparente vers un noeud **worker** exécutant ce service. 

Effectivement, Docker agit en tant que load balancer pour vos services **Swarm**. Les services utilisant le **routing mesh** s'exécutent en mode IP virtuel (VIP). Même un service exécuté sur chaque nœud (au moyen de l'option **--mode global**) utilise le **routing mesh**. Lors de l'utilisation du **routing mesh**, il n'y a aucune garantie de connaître quel noeud Docker va répondre pour les demandes des clients du service.

Pour contourner le **routing mesh**, vous pouvez démarrer un service à l'aide du mode **DNS Round Robin (DNSRR)** en définissant l'option `--endpoint-mode` sur `dnsrr`. Vous devez exécuter votre propre load balancer devant le service. Une requête DNS pour le nom du service sur l'hôte Docker renvoie une liste d'adresses IP pour les nœuds exécutant le service. Configurez votre load balancer pour utiliser cette liste et équilibrer le trafic sur les nœuds.
