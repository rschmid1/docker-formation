## Initialisation d'un cluster Swarm

Les ports suivants doivent être disponibles. Sur certains systèmes, ces ports sont ouverts par défaut.

- Port TCP 2377 pour les communications de gestion de cluster
- Port 7946 TCP et UDP pour la communication entre les nœuds
- Port UDP 4789 pour le trafic réseau en superposition

Si vous envisagez de créer un réseau **overlay** avec encryption (`--opt encrypted`), vous devez également vous assurer que le trafic du protocole IP 50 (ESP) est autorisé.


## Initialiser le premier noeud

Connecter vous sur la machine qui sera le premier noeud **manager** (en ssh par exemple)

Nous allons utiliser la commande : `docker swarm init --advertise-addr <MANAGER-IP>`

les options `--advertise-addr` et `--listen-addr` sont optionnelles mais recommandées. Elles contiennent l'IP du nouveau nœud dans le cluster, les autres nœuds du **Swarm** doivent pouvoir accéder au gestionnaire à l'adresse IP.

```
$ docker swarm init --advertise-addr 192.168.99.100
Swarm initialized: current node (dxn1zf6l61qsb1josjja83ngz) is now a manager.

To add a worker to this swarm, run the following command:

    docker swarm join \
    --token SWMTKN-1-49nj1cmql0jkz5s954yi3oex3nedyz0fb0xx14ie39trti4wxv-8vxv8rssmk743ojnwacrr2e7c \
    192.168.99.100:2377

To add a manager to this swarm, run 'docker swarm join-token manager' and follow the instructions.
```

## Controler l'état du **Swarm**
```
$ docker info
Containers: 2
Running: 0
Paused: 0
Stopped: 2
  ...snip...
Swarm: active
  NodeID: dxn1zf6l61qsb1josjja83ngz
  Is Manager: true
  Managers: 1
  Nodes: 1
  ...snip...

$ docker node ls

ID                           HOSTNAME  STATUS  AVAILABILITY  MANAGER STATUS
dxn1zf6l61qsb1josjja83ngz *  manager1  Ready   Active        Leader

# Le symbole * situé à côté de l’ID de nœud indique que vous êtes actuellement connecté à ce nœud.

```

## Initialiser un autre noeud

Connecter vous sur la machine qui sera le premier noeud **worker** (en ssh par exemple) et exécuter la commande qui vous a été suggéré lors de la commande `docker swarm init` sur le noeud `master`

```
$ docker swarm join \
  --token  SWMTKN-1-49nj1cmql0jkz5s954yi3oex3nedyz0fb0xx14ie39trti4wxv-8vxv8rssmk743ojnwacrr2e7c \
  192.168.99.100:2377

This node joined a swarm as a worker.
```

Si vous ne vous souvenez plus de la commande donnée par le noeud **master** lors de l'initialisation du cluster **Swarm** vous pouvez vous reconnecter sur le noeud **master** et exécuter la commande:
```
$ docker swarm join-token worker

To add a worker to this swarm, run the following command:

    docker swarm join \
    --token SWMTKN-1-49nj1cmql0jkz5s954yi3oex3nedyz0fb0xx14ie39trti4wxv-8vxv8rssmk743ojnwacrr2e7c \
    192.168.99.100:2377
```

## Initialiser un deuxième noeud **worker**
Répétez la commande sur la machine qui sera le deuxième noeud **worker** (en ssh par exemple)

## Afficher l'état du cluster **Swarm**

Reconnectez-vous sur la toute première machine qui se trouve être le noeud **manager** du **Swarm** (en ssh par exemple), et executez la commande ci-dessous pour voir l'état du cluster **Swarm**
```
$ docker node ls
ID                           HOSTNAME  STATUS  AVAILABILITY  MANAGER STATUS
03g1y59jwfg7cf99w4lt0f662    worker2   Ready   Active
9j68exjopxe7wfl6yuxml7a7j    worker1   Ready   Active
dxn1zf6l61qsb1josjja83ngz *  manager1  Ready   Active        Leader
```

Vous avez à présent un cluster **Swarm** initialisé avec un noeud **manager** et deux noeuds **worker**.

Notez cependant que pour un mode en production vous devez avoir au minimum 3 ou 5 noeuds **manager** (quorum) pour la haute disponibilité et permettre la récupération du **Swarm** en cas de disfonctionnement d'un noeud **manager**

## Prochaines étapes
Pour la suite, rendez-vous sur la page suivante [3.0 Sécurité d'un cluster **Swarm**](swarm-security.md)