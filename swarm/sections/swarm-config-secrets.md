
# Docker Configs et Docker Secrets

Lors de l'utilisation de Docker **Swarm**, vous avez la possibilité d'ajouter des configurations et secrets partagés parmi tous les noeuds de votre cluster **Swarm**.

## Docker Configs
Depuis Docker 17.06 introduit Docker Configs, une fonctionalité de Docker **Swarm** qui vous permet de stocker des informations non sensibles, telles que des fichiers de configuration, en dehors de l’image d’un service ou des containers en cours d’exécution. 

Cela vous permet ainsi de garder vos images aussi génériques que possible, sans qu'il soit nécessaire de monter les fichiers de configuration dans des conteneurs ou d'utiliser des variables d'environnement.

Les configurations ne sont pas chiffrées et sont montées directement dans le système de fichiers du container en clair. 

Les configurations peuvent être ajoutées ou supprimées d'un service à tout moment, et les services peuvent partager une configuration. 

Les valeurs de configuration peuvent être sous forme de chaîne de caractère (string) ou du contenu binaire (d'une taille maximale de 500 Ko).

### Options de la commande

```
$ docker config --help

Usage:	docker config COMMAND

Manage Docker configs

Commands:
  create      Create a config from a file or STDIN
  inspect     Display detailed information on one or more configs
  ls          List configs
  rm          Remove one or more configs

Run 'docker config COMMAND --help' for more information on a command.
```

### Création d'une nouvelle config

```
# Création d'une configuration depuis une chaine de caractère
$ echo "ma configuration texte" | docker config create app_config -
7w726n9b7j33m5yaq8le6i9so

# Création d'une configuration depuis un fichier
$ echo "ma configuration depuis un fichier" > myconfig.txt
$ docker config create app_config_from_file myconfig.txt
x4d1apg4n4tox5lo19kjgxd3f

$ docker config ls
ID                          NAME                   CREATED              UPDATED
7w726n9b7j33m5yaq8le6i9so   app_config             About a minute ago   About a minute ago
x4d1apg4n4tox5lo19kjgxd3f   app_config_from_file   15 seconds ago       15 seconds ago
```

### Utilisation de ces configurations dans un service

Démarrons un service en y ajoutant les config **app_config** et **app_config_from_file** crées précédemment.

```
$ docker service create --config app_config --config app_config_from_file --name mynginxservice nginx

$docker exec -it <container_id> cat /app_config
ma configuration texte

$docker exec -it <container_id> cat /app_config_from_file
ma configuration depuis un fichier

```

Comme vous pouvez le remarquer, par défaut les **Docker Configs** sont placés à la racine du container si aucune option supplémentaire n'est donnée. 

Pour indiquer son emplacement dans le container vous devez utiliser la syntaxe étendue:

`--config source=<source_config_name>,target=<target_in_container>,mode=<mode> `:


Exemple:
```
$ docker service create \
     --name nginx \
     --config source=site.conf,target=/etc/nginx/conf.d/site.conf,mode=0440 \
     --publish published=3000,target=443 \
     nginx:latest
```

## Docker Secrets

Dans le cadre d'un Docker **Swarm**, un secret est un bloc de données, tel qu'un mot de passe, une clé privée SSH, un certificat SSL ou un autre élément de données qui ne doit pas être transmis sur un réseau ni stocké non crypté dans un fichier Docker ou dans le code source de votre application. 

Depuis Docker 1.13 et versions ultérieures, vous pouvez utiliser les secrets Docker pour gérer de manière centralisée ces données et les transmettre en toute sécurité aux seuls conteneurs qui doivent y accéder. 

Les secrets sont cryptés pendant le transit et son entreposage dans un Docker **Swarm**. Un secret est uniquement accessible aux services auxquels un accès explicite a été accordé, et uniquement pendant l'exécution des tâches de ces services.

Vous pouvez utiliser des secrets pour gérer les données sensibles dont un conteneur a besoin au moment de l'exécution, mais que vous ne souhaitez pas stocker dans l'image ou dans le source control (Git,SVN,...), tels que:

- Noms d'utilisateur et mots de passe
- Certificats et clés TLS
- Clés SSH
- Autres données importantes telles que le nom d'une base de données ou d'un serveur interne
- Chaînes de caractères génériques ou contenu binaire (taille maximale de 500 Ko)

Un autre cas d'utilisation des secrets est de fournir une couche d'abstraction entre le conteneur et un ensemble d'informations d'identification. 

Par exemple si vous avez plusieurs environnements distincts pour votre application(développement, test, production), chacun de ces environnements peut avoir différentes informations d'identification, stockées dans les **Swarm** de développement, de test et de production avec le même nom de secret. 

Ainsi vos conteneurs doivent seulement connaître le nom du secret pour fonctionner dans les trois environnements.

### Options de la commande
```
docker secret --help

Usage:	docker secret COMMAND

Manage Docker secrets

Commands:
  create      Create a secret from a file or STDIN as content
  inspect     Display detailed information on one or more secrets
  ls          List secrets
  rm          Remove one or more secrets

Run 'docker secret COMMAND --help' for more information on a command.
```

### Création d'un secret
```
$ echo "P@ssw0rD" | docker secret create db_password -
$ docker secret ls
ID                          NAME                DRIVER              CREATED             UPDATED
mn2v3nci5fuk1fpz3rfqev6hd   db_password                             39 minutes ago      39 minutes ago
```

### Utilisation du secret dans un service
```
$ docker service create --secret db_password --name mysecretnginxservice nginx

$ docker exec -it <container_id> cat /run/secrets/db_password
P@ssw0rD


```
### Identifiez le point de montage des secrets

Exécutez la commande ci-dessous pour voir le type de montage qui est effectué par Docker pour les secrets:

```
$ docker exec -it <container_id> mount -l | grep secrets
tmpfs on /run/secrets/db_password type tmpfs (ro,relatime)
```

Vous pouvez remarquer que le secret est monté en mémoire (tmpfs), et ce point de montage en mémoire n'est disponible que lors du cycle de vie du container, il sera détruit lorsque le container sera arrété.

Un exemple complet sur l'utilisation des secrets et configurations avec nginx est disponible sur la [documentation](https://docs.docker.com/engine/swarm/configs/#advanced-example-use-configs-with-a-nginx-service) officielle de Docker

### Utilisation des Docker Configs et Docker Secrets dans un fichier Compose pour à destination d'une Stack
```
version: '3.1'

services:
   db:
     image: mysql:latest
     volumes:
       - db_data:/var/lib/mysql
     environment:
       MYSQL_ROOT_PASSWORD_FILE: /run/secrets/db_root_password
       MYSQL_DATABASE: wordpress
       MYSQL_USER: wordpress
       MYSQL_PASSWORD_FILE: /run/secrets/db_password
     secrets:
       - db_root_password
       - db_password

   wordpress:
     depends_on:
       - db
     image: wordpress:latest
     ports:
       - "8000:80"
     environment:
       WORDPRESS_DB_HOST: db:3306
       WORDPRESS_DB_USER: wordpress
       WORDPRESS_DB_PASSWORD_FILE: /run/secrets/db_password
     secrets:
       - db_password

configs:
    app_config:
      external:
    app_config_from_file:
      file: myconfig.txt

secrets:
   db_password:
     file: db_password.txt
   db_root_password:
     file: db_root_password.txt
   other_secret:
     external:

volumes:
    db_data:
```

## Prochaines étapes
Pour la suite, rendez-vous sur la page suivante [8.0 Réseau Overlay et Load Balancer natif](overlay.md)