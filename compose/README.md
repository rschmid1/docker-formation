## Docker Compose

Docker Compose est un outil qui permet de définir et d'exécuter des applications Docker avec de multiples conteneurs. 

Avec Docker Compose, vous utilisez un fichier YAML pour configurer les services de votre application. Ensuite, avec une seule commande, vous créez et démarrez tous les services de votre configuration.

Pour installer Docker-Compose veuillez vous rendre sur la [page](https://docs.docker.com/compose/install/) dédiée sur le site de Docker.

Exemple de fichier docker-compose.yml pour une application Wordpress
```
version: '3.3'

services:
   db:
     image: mysql:5.7
     volumes:
       - db_data:/var/lib/mysql
     restart: always
     environment:
       MYSQL_ROOT_PASSWORD: somewordpress
       MYSQL_DATABASE: wordpress
       MYSQL_USER: wordpress
       MYSQL_PASSWORD: wordpress

   wordpress:
     depends_on:
       - db
     image: wordpress:latest
     ports:
       - "8000:80"
     restart: always
     environment:
       WORDPRESS_DB_HOST: db:3306
       WORDPRESS_DB_USER: wordpress
       WORDPRESS_DB_PASSWORD: wordpress
volumes:
    db_data:
```

### Déployer avec Docker Compose

Pour démarrer les services définis dans un fichier docker-compose.yml il suffit d'exécuter la commande suivante:
```
docker-compose up -d
```
L'option `-d` permet de lancer le Docker Compose en mode détaché.

### Extinction d'un environnement géré par Docker Compose
```
docker-compose down
# pour supprimer les volumes associès ajouter l'option --volumes
docker-compose down --volumes
```

### Gestion des services Docker Compose

Vous pouvez interagir sur un service donné défini dans un fichier docker-compose comme par exemple stopper un service ou démarrer individuellemet un service:
```
docker-compose stop wordpress
docker-compose start wordpress
docker-compose restart wordpress
...
```

### Build avec Docker Compose

Il est possible d'utiliser docker compose pour construire les images à l'aide de l'instruction `build` dans le fichier docker-compose.yml

l'instruction `build` peut être spécifié comme une chaine de caractères avec un chemin vers un contexte de build
```
version: '3'
services:
  webapp:
    build: ./dir
```

ou comme un objet avec le chemin vers un contexte de build et optionellement un fichier `Dockerfile` et des arguments

```
version: '3'
services:
  webapp:
    build:
      context: ./dir
      dockerfile: Dockerfile-alternate
      args:
        buildno: 1
```

Si vous indiquez l'instruction `ìmage` en même temps que l'instruction `build`, alors docker-compose nommera l'image résultant du build comme spécifié dans l'instruction `image`

```
build: ./dir
image: webapp:tag
```



`docker-compose build`
