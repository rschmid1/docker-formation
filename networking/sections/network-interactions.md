## Commandes d'intéractions avec les réseaux

### Créeation d'un réseau
```
$ docker network create --help
 
$ docker network create mynet1
7e328f3f469921cf5f2b0e032b4b7efb960dc3c35088818c6c8fe240ca85e399
 
$ docker network ls
NETWORK ID          NAME                      DRIVER              SCOPE
d039b68eb631        bridge                    bridge              local
ffc498e32fa9        host                      host                local
jwz93ivupcs4        ingress                   overlay             swarm
7e328f3f4699        mynet1                    bridge              local
975e3ead96c6        none                      null                local


$ docker network inspect mynet1
[
    {
        "Name": "mynet1",
        "Id": "7e328f3f469921cf5f2b0e032b4b7efb960dc3c35088818c6c8fe240ca85e399",
        "Created": "2018-11-11T14:16:01.7829096Z",
        "Scope": "local",
        "Driver": "bridge",
        "EnableIPv6": false,
        "IPAM": {
            "Driver": "default",
            "Options": {},
            "Config": [
                {
                    "Subnet": "172.20.0.0/16",
                    "Gateway": "172.20.0.1"
                }
            ]
        },
        "Internal": false,
        "Attachable": false,
        "Ingress": false,
        "ConfigFrom": {
            "Network": ""
        },
        "ConfigOnly": false,
        "Containers": {},
        "Options": {},
        "Labels": {}
    }
]

```

Comme vous pouvez le voir, le réseau a comme sous-réseau 172.20.0.0/16, et comme gateway 172.20.0.1. 

Les conteneurs auront donc des IP attribuées entre 172.20.0.2 et 172.20.0.254 (172.20.0.255 étant le broadcast), donc au total 65534 addresses diponible (65536 - (gateway et brodcast))

```
$ docker network create --subnet 10.0.50.0/24 --gateway 10.0.50.254 --ip-range 10.0.50.0/28 mynet2
3190ef04adf8a9a87dab9757e60ac0d96f99f79597dd4f28d7d619edf3c38a88

$ docker network inspect mynet2
[
    {
        "Name": "mynet2",
        "Id": "3190ef04adf8a9a87dab9757e60ac0d96f99f79597dd4f28d7d619edf3c38a88",
        "Created": "2018-11-11T14:19:10.839984Z",
        "Scope": "local",
        "Driver": "bridge",
        "EnableIPv6": false,
        "IPAM": {
            "Driver": "default",
            "Options": {},
            "Config": [
                {
                    "Subnet": "10.0.50.0/24",
                    "IPRange": "10.0.50.0/28",
                    "Gateway": "10.0.50.254"
                }
            ]
        },
        "Internal": false,
        "Attachable": false,
        "Ingress": false,
        "ConfigFrom": {
            "Network": ""
        },
        "ConfigOnly": false,
        "Containers": {},
        "Options": {},
        "Labels": {}
    }
]
```

## Attacher le réseau à un container
```
$ docker run -d --network mynet2 --name mycontainer alpine:3.8 tail -f /dev/null
b190494b91e6c67beeda289339d4a4e24bdda48818b15a31e23e35b6b6d3cb55

$ docker exec -it mycontainer ifconfig
eth0      Link encap:Ethernet  HWaddr 02:42:0A:00:32:01  
          inet addr:10.0.50.1  Bcast:10.0.50.255  Mask:255.255.255.0
          UP BROADCAST RUNNING MULTICAST  MTU:1500  Metric:1
          RX packets:11 errors:0 dropped:0 overruns:0 frame:0
          TX packets:0 errors:0 dropped:0 overruns:0 carrier:0
          collisions:0 txqueuelen:0 
          RX bytes:898 (898.0 B)  TX bytes:0 (0.0 B)

lo        Link encap:Local Loopback  
          inet addr:127.0.0.1  Mask:255.0.0.0
          UP LOOPBACK RUNNING  MTU:65536  Metric:1
          RX packets:0 errors:0 dropped:0 overruns:0 frame:0
          TX packets:0 errors:0 dropped:0 overruns:0 carrier:0
          collisions:0 txqueuelen:1 
          RX bytes:0 (0.0 B)  TX bytes:0 (0.0 B)


$ docker network connect mynet1 mycontainer

$ docker exec -it mycontainer ifconfig
docker exec -it mycontainer ifconfig
eth0      Link encap:Ethernet  HWaddr 02:42:0A:00:32:01  
          inet addr:10.0.50.1  Bcast:10.0.50.255  Mask:255.255.255.0
          UP BROADCAST RUNNING MULTICAST  MTU:1500  Metric:1
          RX packets:12 errors:0 dropped:0 overruns:0 frame:0
          TX packets:0 errors:0 dropped:0 overruns:0 carrier:0
          collisions:0 txqueuelen:0 
          RX bytes:968 (968.0 B)  TX bytes:0 (0.0 B)

eth1      Link encap:Ethernet  HWaddr 02:42:AC:14:00:02  
          inet addr:172.20.0.2  Bcast:172.20.255.255  Mask:255.255.0.0
          UP BROADCAST RUNNING MULTICAST  MTU:1500  Metric:1
          RX packets:16 errors:0 dropped:0 overruns:0 frame:0
          TX packets:0 errors:0 dropped:0 overruns:0 carrier:0
          collisions:0 txqueuelen:0 
          RX bytes:1296 (1.2 KiB)  TX bytes:0 (0.0 B)

lo        Link encap:Local Loopback  
          inet addr:127.0.0.1  Mask:255.0.0.0
          UP LOOPBACK RUNNING  MTU:65536  Metric:1
          RX packets:0 errors:0 dropped:0 overruns:0 frame:0
          TX packets:0 errors:0 dropped:0 overruns:0 carrier:0
          collisions:0 txqueuelen:1 
          RX bytes:0 (0.0 B)  TX bytes:0 (0.0 B)


$ docker network inspect mynet1
[
    {
        "Name": "mynet1",
        "Id": "7e328f3f469921cf5f2b0e032b4b7efb960dc3c35088818c6c8fe240ca85e399",
        "Created": "2018-11-11T14:16:01.7829096Z",
        "Scope": "local",
        "Driver": "bridge",
        "EnableIPv6": false,
        "IPAM": {
            "Driver": "default",
            "Options": {},
            "Config": [
                {
                    "Subnet": "172.20.0.0/16",
                    "Gateway": "172.20.0.1"
                }
            ]
        },
        "Internal": false,
        "Attachable": false,
        "Ingress": false,
        "ConfigFrom": {
            "Network": ""
        },
        "ConfigOnly": false,
        "Containers": {
            "b190494b91e6c67beeda289339d4a4e24bdda48818b15a31e23e35b6b6d3cb55": {
                "Name": "mycontainer",
                "EndpointID": "9bd133aa82c335beafb1250c9ada7a17bb61df90f740802f9d07f770349928c9",
                "MacAddress": "02:42:ac:14:00:02",
                "IPv4Address": "172.20.0.2/16",
                "IPv6Address": ""
            }
        },
        "Options": {},
        "Labels": {}
    }
]

 ```