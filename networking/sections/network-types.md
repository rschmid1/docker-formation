
## Docker et les types de réseaux

De base, il existe cinq types de réseaux (networks) gérés par des pilotes de réseaux (network drivers):
 
- **bridge** : réseau par défaut, crée un réseau interne pour vos conteneurs
- **host** : ce type de réseau permet au conteneur d'avoir la même interface que l'hôte
- **none** : comme le nom l'indique, aucun réseau pour les conteneurs et utilisé en conjonction avec un pilote réseau personalisé (custom)
- **macvlan** : permet d'assigner des addresses MAC aux containers pour qu'ils apparaissent sur le réseau comme des machines physiques sur votre réseau. Le Docker Daemon route le traffic sur les containers par leur adresse MAC. 
- **overlay** : réseau interne entre plusieurs hôtes.

Il existe d'autres types de pilote réseaux developpés par des parties tierces disponible sur sur le [Docker Store](https://store.docker.com/search?category=network&q=&type=plugin)


En résumé:
- Les réseaux de type **bridge** définis par l'utilisateur sont préférables lorsque plusieurs conteneurs doivent communiquer sur le même hôte Docker.
- Les réseaux de type **host** sont préférables lorsque la stack réseau ne doit pas être isolée de l'hôte Docker, mais que vous souhaitez isoler d'autres aspects du conteneur.
- Les réseaux de type **overlay** conviennent mieux lorsque vous avez besoin de conteneurs s'exécutant sur différents hôtes Docker pour communiquer ou lorsque plusieurs applications fonctionnent ensemble à l'aide de services swarm.
- Les réseaux Macvlan conviennent mieux lorsque vous migrez depuis une configuration de machine virtuelle ou que vos conteneurs doivent ressembler à des hôtes physiques sur votre réseau, chacun possédant une adresse MAC unique.
- Les plugins réseau de parties tierce vous permettent d’intégrer Docker à des stacks réseaus spécialisées.
