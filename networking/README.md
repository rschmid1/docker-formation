## Réseau

Cette partie comporte les sections suivantes:

* [1.0 Type de réseaux](sections/network-types.md)
* [2.0 Interactions avec les réseaux](sections/network-interactions.md)
