# Stockage

## Stockage dans le container

Par défaut, tous les fichiers créés dans un conteneur sont stockés sur une couche (layer) de conteneur en écriture. 

Cela signifie que:

- Les données ne persistent pas lorsque ce conteneur n'est plus en cours d'exécution et il peut être difficile d'extraire les données du conteneur si un autre processus en a besoin.
- La couche inscriptible d’un conteneur est étroitement liée à la machine hôte sur laquelle le conteneur est exécuté. Vous ne pouvez pas déplacer facilement les données ailleurs.
- L'écriture dans la couche en écriture d'un conteneur nécessite un pilote de stockage pour gérer le système de fichiers. Le pilote de stockage fournit un système de fichiers appelé **union filesystem** utilisant le noyau Linux. Cette abstraction supplémentaire réduit les performances par rapport à l'utilisation de volumes de données, qui écrivent directement dans le système de fichiers hôte.

![Thin-rw Layer](thin-rw-layer.png)

![Thin-rw Layer multi containers](thin-rw-layer-multicontainers.png)

### La stratégie de **copy-on-write**(CoW)
Le ***copy-on-write** est une stratégie de partage et de copie de fichiers pour une efficacité maximale. Si un fichier ou un répertoire existe dans une couche inférieure de l'image et qu'une autre couche (y compris la couche accessible en écriture) nécessite un accès en lecture, il utilise uniquement le fichier existant. 

La première fois qu'une autre couche (layer) doit modifier le fichier (lors de la création de l'image ou de l'exécution du conteneur), le fichier est copié dans la couche inscriptible et modifié. Cela minimise les entrés/sorties (I/O) et la taille de chacune des couches suivantes.

Le fonctionnement du ***copy-on-write*** diffère selon le pilote de stockage utilisé.

### Types de pilotes de stockage (storage drivers)

| Pilote de stockage | Système de fichiers supportés |
| ------------------ | ----------------------------- | 
| overlay, overlay2  | ext4, xfs |
| aufs | ext4, xfs |
| devicemapper | direct-lvm |
| btrfs | btrfs
| zfs | zfs


### Recommendations de pilotes de stockage
| Distribution Linux | Pilote de stockage recommandé |
| ------------------ | ----------------------------- |
| Docker CE on Ubuntu |	aufs, devicemapper, overlay2 (Ubuntu 14.04.4 or later, 16.04 or later), overlay, zfs, vfs |
| Docker CE on Debian | aufs, devicemapper, overlay2 (Debian Stretch), overlay, vfs |
| Docker CE on CentOS |	devicemapper, vfs |
| Docker CE on Fedora |	devicemapper, overlay2 (Fedora 26 or later, experimental), overlay (experimental), vfs |

Avec les versions récentes de Docker, le type de pilote par défaut (suivant les Distributions Linux) est **overlay2**

Plus de détails sur les pilotes de stockage disponible sur la [page](https://docs.docker.com/storage/storagedriver/) dédiée sur le site de Docker

## Comment choisir ?
Chaque pilote de stockage a ses propres caractéristiques de performances qui le rendent plus ou moins adapté à différentes charges de travail. En considérant les généralisations suivantes:

- aufs, overlay et overlay2 fonctionnent tous au niveau du fichier plutôt qu'au niveau du bloc. Ceci utilise plus efficacement la mémoire, mais la couche d’inscription du conteneur peut devenir assez volumineuse dans les charges de travail nécessitant beaucoup d’écriture.
- Les pilotes de stockage de niveau bloc tels que devicemapper, btrfs et zfs fonctionnent mieux pour les charges de travail nécessitant beaucoup d’écriture (mais pas aussi bien que les volumes Docker).
- Pour un grand nombre de petites écritures ou de conteneurs avec plusieurs couches ou systèmes de fichiers profonds, le type `overlay` peut être meilleure que le type `overlay2`.
- btrfs et zfs nécessitent beaucoup de mémoire.
- zfs est un bon choix pour les charges de travail à haute densité telles que dans un PaaS.

## Stockage en dehors du container

Docker dispose de deux options pour les conteneurs afin de stocker des fichiers sur la machine hôte et donc en dehors du container. Ainsi, les fichiers sont persistants même après l’arrêt et destruction du conteneur:
- Volumes et 
- Points de montage (**bind mounts**). 
- Si vous utilisez Docker sous Linux, vous pouvez également utiliser un point de montage **tmpfs**.

![Type of mounts](types-of-mounts-bind.png)

- Les volumes sont stockés dans une partie du système de fichiers hôte géré par Docker (`/var/lib/docker/volumes/` sous Linux). Les processus non-Docker ne doivent pas modifier cette partie du système de fichiers. Les volumes constituent le meilleur moyen de conserver des données dans Docker.

- Les points de montage (**bind mount**) peuvent être stockés n’importe où sur le système hôte. Il peut même s'agir de fichiers système ou de répertoires importants. Les processus non-Docker sur l'hôte Docker ou un conteneur Docker peuvent les modifier à tout moment.

- Les montages tmpfs sont uniquement stockés dans la mémoire du système hôte et ne sont jamais écrits dans le système de fichiers du système hôte.

## Prochaines étapes
Pour la suite, rendez-vous sur la page suivante [2.0 Points de montage](mount.md)
