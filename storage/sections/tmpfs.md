# Montage tmpfs (tmpfs mounts)
Les **volumes** et les points de montages (**bind mount**) vous permettent de partager des fichiers entre la machine hôte et le conteneur afin que vous puissiez conserver des données même après l'arrêt du conteneur.

Si vous utilisez Docker sous Linux, vous avez une troisième option: **montage tmpfs (tmpfs mounts)**. Lorsque vous créez un conteneur avec un montage **tmpfs**, ce dernier peut créer des fichiers en dehors de la couche inscriptible du conteneur.

![Type of mounts](types-of-mounts-bind.png)

Contrairement aux **volumes** et aux montages de liaison, un montage **tmpfs** est temporaire et ne subsiste que dans la mémoire de l'hôte. Lorsque le conteneur s’arrête, le montage tmpfs est supprimé et les fichiers qui y sont écrits ne seront plus conservés.

Un montage tmpfs n'est pas conservé sur le disque, ni sur l'hôte Docker, ni dans un conteneur. Il peut être utilisé par un conteneur pendant sa durée de vie pour stocker des informations sensibles ou un état non persistant. Par exemple, en interne, les services swarm utilisent des montages tmpfs pour monter des secrets dans les conteneurs d’un service.

## Cas d'utilisation pour les montages tmpfs
Les montages tmpfs peuvent être utilisés dans les cas où vous ne souhaitez pas que les données persistent sur la machine hôte ou dans le conteneur. Cela peut être pour des raisons de sécurité ou pour protéger les performances du conteneur lorsque votre application doit écrire un grand volume de données d'état non persistantes.

## Limitations du montage **tmpfs**

- Contrairement aux **volumes** et aux **points de montage (bind mount)**, vous ne pouvez pas partager les **montages tmpfs** entre les conteneurs.
- Cette fonctionnalité est disponible uniquement si vous utilisez Docker sous Linux.

Pour plus de détails concernant les volumes vous pouvez vous rendre sur cette [page](https://docs.docker.com/storage/tmpfs/) dédiée sur le site de Docker.
