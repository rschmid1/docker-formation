# Volumes

Les volumes constituent le mécanisme privilégié pour la persistance des données générées par les conteneurs Docker et utilisées par ceux-ci. 

![Type of mounts](types-of-mounts-bind.png)

Les volumes sont stockés dans une partie du système de fichiers hôte géré par Docker (`/var/lib/docker/volumes/` sous Linux)

Bien que les points de montage dépendent de la structure de répertoires de la machine hôte, les volumes sont entièrement gérés par Docker. Les volumes présentent plusieurs avantages par rapport aux supports de liaison:

- Les volumes sont plus faciles à sauvegarder ou à migrer que les montages liés.
- Vous pouvez gérer des volumes à l'aide des commandes CLI de Docker ou de l'API Docker.
- Les volumes fonctionnent à la fois sur les conteneurs Linux et Windows.
- Les volumes peuvent être partagés de manière plus sécurisée entre plusieurs conteneurs.
- Les pilotes de volume vous permettent de stocker des volumes sur des hôtes distants ou des fournisseurs de cloud, pour chiffrer le contenu des volumes ou pour ajouter d'autres fonctionnalités.
- Le contenu des nouveaux volumes peut être pré-rempli par un conteneur.
- De plus, les volumes constituent souvent un meilleur choix que de conserver des données dans la couche inscriptible d’un conteneur, car un volume n’augmente pas la taille des conteneurs qui l’utilisent et son contenu existe en dehors du cycle de vie d’un conteneur donné.

## Conseils d'utilisation sur les points de montages ou des volumes

Si vous utilisez des **points montages** ou des **volumes**, tenez compte des points suivants:

- Si vous montez un **volume** vide dans un répertoire du conteneur dans lequel des fichiers ou des répertoires existent, ces fichiers ou répertoires sont propagés (copiés) dans le **volume**. De même, si vous démarrez un conteneur et spécifiez un **volume** qui n'existe pas déjà, un **volume** vide est créé pour vous. C'est un bon moyen de pré-renseigner les données nécessaires à un autre conteneur.

- Si vous montez un **point de montage** ou un **volume** non vide dans un répertoire du conteneur dans lequel se trouvent certains fichiers ou répertoires, ces fichiers ou répertoires sont masqués par le montage. Les fichiers masqués ne sont ni supprimés ni modifiés, mais ne sont pas accessibles tant que le **point montage** ou le **volume** est monté.

## Commandes

### Création d'un **volume**

`docker volume create <nom-volume>`

### Inspection d'un volume
`docker volume inspect <nom-volume>`

Exemple:
```
$docker volume create mysql-volume
mysql-volume
$docker volume inspect mysql-volume
[
    {
        "CreatedAt": "2018-11-10T18:13:43Z",
        "Driver": "local",
        "Labels": {},
        "Mountpoint": "/var/lib/docker/volumes/mysql-volume/_data",
        "Name": "mysql-volume",
        "Options": {},
        "Scope": "local"
    }
]
```

### Démarrer un container avec montage d'un volume
```
$ docker run --name mysql -v mysql-data:/var/lib/mysql -e MYSQL_ROOT_PASSWORD=p@ssw0rd -d mysql:8.0
ca44ea0101fdca79468b6e60a69cf9afa8653f1762edae05a79a3a8d4925ec8e

$ docker volume inspect mysql-data
[
    {
        "CreatedAt": "2018-11-11T14:54:08Z",
        "Driver": "local",
        "Labels": null,
        "Mountpoint": "/var/lib/docker/volumes/mysql-data/_data",
        "Name": "mysql-data",
        "Options": null,
        "Scope": "local"
    }
]


$ docker volume ls
DRIVER              VOLUME NAME
local               6c44870c634da6a08de7c0b4b953e0f45b9d0655bc4d2666df512a6fec49c74e
local               c46d7f68776f4837117f94a52c3a14737289727755b5eaa08c3e9c485a380ba8
local               mysql-data
local               portainer_data
...
```

Notez qu'à l'origine, l'option `-v` ou `--volume` était utilisé pour les conteneurs autonomes et l'option `--mount` pour les services **Swarm**. 

Cependant, à partir de Docker 17.06, vous pouvez également utiliser `--mount` avec des container autonomes. En général, `--mount` est plus explicite et verbeux. 

La grande différence est que la syntaxe `-v` combine toutes les options dans un seul champ, tandis que la syntaxe `--mount` les sépare.

Exemple:
```
docker run -d --mount type=volume,target=/data
docker run -d --mount type=bind,src=/data,dst=/data
```

## Exemple volume et multi-containers

```
$ docker run -d --rm --name mynginx1 -p 7800:80 -v nginx-html:/usr/share/nginx/html/ nginx
9f0962f95cb5c51962d0c0ac87d77fca21cd4132b1068c87ede8b4bf84683bc6

$ curl localhost:7800
<!DOCTYPE html>
<html>
<head>
<title>Welcome to nginx!</title>
<style>
    body {
        width: 35em;
        margin: 0 auto;
        font-family: Tahoma, Verdana, Arial, sans-serif;
    }
</style>
</head>
<body>
<h1>Welcome to nginx!</h1>
<p>If you see this page, the nginx web server is successfully installed and
working. Further configuration is required.</p>

<p>For online documentation and support please refer to
<a href="http://nginx.org/">nginx.org</a>.<br/>
Commercial support is available at
<a href="http://nginx.com/">nginx.com</a>.</p>

<p><em>Thank you for using nginx.</em></p>
</body>
</html>

$ docker exec -it mynginx1 bash
echo 'Hello World' > /usr/share/nginx/html/index.html
CTRL+C
$ curl localhost:7800
Hello World

# Démarrage d'un deuxième container utilisant le même module
$ docker run -d --rm --name mynginx2 -p 7900:80 -v nginx-html:/usr/share/nginx/html/ nginx
387050ab3555d89f20adcd60285f902bb0de3cc24852614e5da41185e62e14dd

$ curl localhost:7900
Hello World

# Arrêt du premier container
$ docker stop mynginx1

$ curl localhost:7900
Hello World

```

Pour plus de détails concernant les volumes vous pouvez vous rendre sur cette [page](https://docs.docker.com/storage/volumes/) dédiée sur le site de Docker.

## Prochaines étapes
Pour la suite, rendez-vous sur la page suivante [4.0 Points de montage en mémoire](tmpfs.md)

