# Points de montage (bind mount)
Disponible depuis les débuts de Docker. Les points de montage (**bind mount**) ont des fonctionnalités limitées par rapport aux volumes. 

Lorsque vous utilisez un point de montage, un fichier ou un répertoire sur la machine hôte est monté dans un conteneur. Le fichier ou le répertoire est référencé par son chemin complet sur la machine hôte. Le fichier ou le répertoire n'a pas besoin d'exister déjà sur l'hôte Docker. Il est créé à la demande s'il n'existe pas encore.

![bind mounts](types-of-mounts-bind.png)

Les points de montage sont très performants, mais ils reposent sur le système de fichiers de la machine hôte disposant d’une structure de répertoires spécifique. 

Si vous développez de nouvelles applications Docker, envisagez plutôt d'utiliser des **volumes**. Vous ne pouvez pas utiliser les commandes CLI de Docker pour gérer directement les montages de liaisons.

### Avertissement: les points de montages permettent l’accès aux fichiers sensibles

L'un des effets secondaires de l'utilisation de points de montage, pour le meilleur ou pour le pire, est que vous pouvez modifier le système de fichiers hôte via des processus s'exécutant dans un conteneur, notamment la création, la modification ou la suppression de fichiers système ou de répertoires importants. 

Il s'agit d'une capacité puissante qui peut avoir des conséquences sur la sécurité, notamment sur les processus autres que Docker sur le système hôte.

## Commandes

### Démarrage d'un container avec un point de montage
```
# avec option --mount
$ docker run -d \
  -it \
  --name devtest \
  --mount type=bind,source="$(pwd)"/target,target=/app \
  nginx:latest

# avec option -v
$ docker run -d \
  -it \
  --name devtest \
  -v "$(pwd)"/target:/app \
  nginx:latest
```
### Vérification du point de montage
```
$ docker inspect devtest
...
"Mounts": [
    {
        "Type": "bind",
        "Source": "/tmp/source/target",
        "Destination": "/app",
        "Mode": "",
        "RW": true,
        "Propagation": "rprivate"
    }
],
...
```

Pour plus de détails concernant les points de montage vous pouvez vous rendre sur cette [page](https://docs.docker.com/storage/bind-mounts/) dédiée sur le site de Docker.

## Prochaines étapes
Pour la suite, rendez-vous sur la page suivante [3.0 Volumes](volumes.md)